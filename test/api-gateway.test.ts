import {getAllApiKeysTags} from '../src/api-gateway'
import {GetApiKeysCommand} from '@aws-sdk/client-api-gateway'

describe('getAllApiKeysTags', () => {
    it('should return all the api keys', async () => {
        const client = {
            send: jest.fn()
                .mockResolvedValueOnce({
                    items: [{
                        id: 'key1',
                        tags: {
                            APP: 'LINK_GEN'
                        }
                    }],
                    position: '1234'
                })
                .mockResolvedValueOnce({
                    items: [{
                        id: 'key2',
                        tags: {
                            APP: 'SHORTENER'
                        }
                    }]
                })
        }

        const maybeKeys = await getAllApiKeysTags(client as any)()

        expect(maybeKeys.isPresent).toBeTruthy()
        expect(maybeKeys.get()).toStrictEqual([
            {
                keyId: 'key1',
                tags: {
                    APP: 'LINK_GEN'
                }
            },
            {
                keyId: 'key2',
                tags: {
                    APP: 'SHORTENER'
                }
            }
        ])

        expect(client.send).toBeCalledTimes(2)
        expect(client.send.mock.calls[0][0] instanceof GetApiKeysCommand).toBeTruthy()
        expect(client.send.mock.calls[0][0].input).toStrictEqual({
            limit: 500,
            position: undefined
        })
        expect(client.send.mock.calls[1][0] instanceof GetApiKeysCommand).toBeTruthy()
        expect(client.send.mock.calls[1][0].input).toStrictEqual({
            limit: 500,
            position: '1234'
        })
    })
})
