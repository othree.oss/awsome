import {fragment} from '../src/utils'

describe('fragment', () => {
    it('should fragment an array into another array of the specified size', () => {
        const input = [1,2,3,4,5,6,7,8,9]

        const fragmentedArray = fragment(input, 2)

        expect(fragmentedArray).toStrictEqual([[1,2], [3,4], [5,6], [7,8], [9]])
    })

    it('should fragment an array into another array of the specified size starting on the specified index', () => {
        const input = [1,2,3,4,5,6,7,8,9]

        const fragmentedArray = fragment(input, 2, 2)

        expect(fragmentedArray).toStrictEqual([[3,4], [5,6], [7,8], [9]])
    })

    it('should fragment an array into another array of the specified size starting on the specified index and the initialized array', () => {
        const input = [1,2,3,4,5,6,7,8,9]

        const fragmentedArray = fragment(input, 2, 2, [['a','b']])

        expect(fragmentedArray).toStrictEqual([['a','b'],[3,4], [5,6], [7,8], [9]])
    })
})
