import {DeleteObjectCommand, GetObjectCommand, ListObjectsCommand, PutObjectCommand, GetBucketLocationCommand} from '@aws-sdk/client-s3'
import {Readable} from 'stream'
import {s3} from '../src'
import {S3Configuration} from '../src/s3'

describe('S3', () => {
    const configuration: S3Configuration = {
        bucket: 'someBucket'
    }
    describe('get', () => {
        const data = new Readable()

        it('should get the object from S3', async () => {
            const s3Client = {
                send: jest.fn().mockReturnValue(Promise.resolve({
                    Body: data,
                    ContentType: 'application/json',
                    ContentLength: 100
                }))
            }

            const maybeObject = await s3.get(s3Client as any, configuration)('someFile.json')
            expect(maybeObject.isPresent).toBeTruthy()
            expect(maybeObject.get()).toStrictEqual({
                body: data,
                contentType: 'application/json',
                contentLength: 100
            })

            const sendMock = s3Client.send.mock
            expect(sendMock.calls.length).toEqual(1)
            expect(sendMock.calls[0][0] instanceof GetObjectCommand).toBeTruthy()
            expect(sendMock.calls[0][0].input).toStrictEqual({
                Bucket: 'someBucket',
                Key: 'someFile.json'
            })
        })
    })

    describe('getPublicUrl', () => {

        it('should get the object public url from S3', async () => {
            const s3Client = {
                send: jest.fn().mockReturnValue(Promise.resolve({
                    LocationConstraint: 'us-west-2'
                }))
            }

            const maybeObject = await s3.getPublicUrl(s3Client as any, configuration)('someFile.json')
            expect(maybeObject.isPresent).toBeTruthy()
            expect(maybeObject.get()).toStrictEqual('https://someBucket.s3.us-west-2.amazonaws.com/someFile.json')

            const sendMock = s3Client.send.mock
            expect(sendMock.calls.length).toEqual(1)
            expect(sendMock.calls[0][0] instanceof GetBucketLocationCommand).toBeTruthy()
            expect(sendMock.calls[0][0].input).toStrictEqual({
                Bucket: 'someBucket'
            })
        })

        it('should get the object public url from S3 and use the default region if LocationConstraint is not returned', async () => {
            const s3Client = {
                send: jest.fn().mockReturnValue(Promise.resolve({
                    LocationConstraint: undefined
                }))
            }

            const maybeObject = await s3.getPublicUrl(s3Client as any, configuration)('someFile.json')
            maybeObject.get()
            expect(maybeObject.isPresent).toBeTruthy()
            expect(maybeObject.get()).toStrictEqual('https://someBucket.s3.us-east-1.amazonaws.com/someFile.json')

            const sendMock = s3Client.send.mock
            expect(sendMock.calls.length).toEqual(1)
            expect(sendMock.calls[0][0] instanceof GetBucketLocationCommand).toBeTruthy()
            expect(sendMock.calls[0][0].input).toStrictEqual({
                Bucket: 'someBucket'
            })
        })
    })

    describe('list', () => {
        it('should list the objects on the provided path', async () => {
            const date = new Date()

            const s3Client = {
                send: jest.fn().mockResolvedValue({
                    Contents: [{
                        Key: 'file.json',
                        Size: 100,
                        LastModified: date
                    }]
                })
            }

            const maybeObjects = await s3.list(s3Client as any, configuration)('test/configuration')

            expect(maybeObjects.isPresent).toBeTruthy()
            expect(maybeObjects.get()).toStrictEqual([{
                key: 'file.json',
                size: 100,
                lastModifiedDate: date
            }])

            const sendMock = s3Client.send.mock
            expect(sendMock.calls.length).toEqual(1)
            expect(sendMock.calls[0][0] instanceof ListObjectsCommand).toBeTruthy()
            expect(sendMock.calls[0][0].input).toStrictEqual({
                Bucket: 'someBucket',
                Prefix: 'test/configuration'
            })
        })
    })

    describe('put', () => {
        const data = new Readable()

        it('should put an object to S3', async () => {
            const s3Client = {
                send: jest.fn().mockReturnValue(Promise.resolve(true))
            }

            const maybePutObject = await s3.put(s3Client as any, configuration)('someFile.json', {
                body: data,
                contentType: 'application/json',
                contentLength: 100
            })

            expect(maybePutObject.isPresent).toBeTruthy()
            expect(maybePutObject.get()).toBeTruthy()

            const sendMock = s3Client.send.mock
            expect(sendMock.calls.length).toEqual(1)
            expect(sendMock.calls[0][0] instanceof PutObjectCommand).toBeTruthy()
            expect(sendMock.calls[0][0].input).toStrictEqual({
                Body: data,
                Bucket: 'someBucket',
                Key: 'someFile.json',
                ContentType: 'application/json',
                ContentLength: 100
            })
        })
    })

    describe('delete', () => {
        it('should delete the object from S3', async () => {
            const s3Client = {
                send: jest.fn().mockReturnValue(Promise.resolve(true))
            }

            const maybeDeleted = await s3.deleteObject(s3Client as any, configuration)('someFile.json')

            expect(maybeDeleted.isPresent).toBeTruthy()
            expect(maybeDeleted.get()).toBeTruthy()

            const sendMock = s3Client.send.mock
            expect(sendMock.calls.length).toEqual(1)
            expect(sendMock.calls[0][0] instanceof DeleteObjectCommand).toBeTruthy()
            expect(sendMock.calls[0][0].input).toStrictEqual({
                Bucket: 'someBucket',
                Key: 'someFile.json'
            })
        })
    })
})
