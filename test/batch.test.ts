import {batch} from '../src'

describe('submitJob', () => {
    it('should submit a job', async () => {
        const sendMock = jest.fn().mockResolvedValue({
            $metadata: {
                httpStatusCode: 200
            },
            jobId: '0001',
            jobArn: 'aws:batch:job:testJob',
            jobName: 'TestJob'
        })

        const client = {
            send: sendMock
        } as any

        const request: batch.SubmitJobRequest = {
            jobArn: 'aws:batch:job:testJob',
            jobDefinitionArn: 'aws:batch:definition:testDef',
            jobQueueArn: 'aws:batch:queue:testQueue'
        }

        const maybeSubmittedJob = await batch.submitJob(client)(request)

        expect(maybeSubmittedJob.isPresent).toBeTruthy()
        expect(maybeSubmittedJob.get()).toStrictEqual({
            jobId: '0001',
            jobArn: 'aws:batch:job:testJob',
            jobName: 'TestJob'
        })

        expect(sendMock).toHaveBeenCalledTimes(1)
        expect(sendMock.mock.calls[0][0].input).toStrictEqual({
            jobName: 'aws:batch:job:testJob',
            jobDefinition: 'aws:batch:definition:testDef',
            jobQueue: 'aws:batch:queue:testQueue'
        })
    })

    it('should try to submit a job and return an empty optional if an error occurred', async () => {
        const sendMock = jest.fn().mockResolvedValue({
            $metadata: {
                httpStatusCode: 500
            }
        })

        const client = {
            send: sendMock
        } as any

        const request: batch.SubmitJobRequest = {
            jobArn: 'aws:batch:job:testJob',
            jobDefinitionArn: 'aws:batch:definition:testDef',
            jobQueueArn: 'aws:batch:queue:testQueue'
        }

        const maybeSubmittedJob = await batch.submitJob(client)(request)

        expect(maybeSubmittedJob.isEmpty).toBeTruthy()
        expect(maybeSubmittedJob.getError()).toStrictEqual(new Error('Unexpected while submitting job, code: 500'))

        expect(sendMock).toHaveBeenCalledTimes(1)
        expect(sendMock.mock.calls[0][0].input).toStrictEqual({
            jobName: 'aws:batch:job:testJob',
            jobDefinition: 'aws:batch:definition:testDef',
            jobQueue: 'aws:batch:queue:testQueue'
        })
    })
})

describe('getJobStatus', () => {
    it('should get the job status', async () => {
        const sendMock = jest.fn().mockResolvedValue({
            $metadata: {
                httpStatusCode: 200
            },
            jobs: [{
                status: 'RUNNING'
            }]
        })

        const client = {
            send: sendMock
        } as any

        const request: batch.JobStatusRequest = {
            jobId: '0001',
        }

        const maybeJobStatus = await batch.getJobStatus(client)(request)

        expect(maybeJobStatus.isPresent).toBeTruthy()
        expect(maybeJobStatus.get()).toStrictEqual(batch.JobStatus.RUNNING)

        expect(sendMock).toHaveBeenCalledTimes(1)
        expect(sendMock.mock.calls[0][0].input).toStrictEqual({
            jobs: ['0001']
        })
    })

    it('should try to get the job status and return an empty optional if an error occurred', async () => {
        const sendMock = jest.fn().mockResolvedValue({
            $metadata: {
                httpStatusCode: 500
            }
        })

        const client = {
            send: sendMock
        } as any

        const request: batch.JobStatusRequest = {
            jobId: '0001',
        }

        const maybeJobStatus = await batch.getJobStatus(client)(request)

        expect(maybeJobStatus.isEmpty).toBeTruthy()
        expect(maybeJobStatus.getError()).toStrictEqual(new Error('Unexpected while getting job status, code: 500'))

        expect(sendMock).toHaveBeenCalledTimes(1)
        expect(sendMock.mock.calls[0][0].input).toStrictEqual({
            jobs: ['0001']
        })
    })

    it('should try to get the job status and return an empty optional if an empty job list is returned', async () => {
        const sendMock = jest.fn().mockResolvedValue({
            $metadata: {
                httpStatusCode: 200
            },
            jobs: []
        })

        const client = {
            send: sendMock
        } as any

        const request: batch.JobStatusRequest = {
            jobId: '0001',
        }

        const maybeJobStatus = await batch.getJobStatus(client)(request)

        expect(maybeJobStatus.isEmpty).toBeTruthy()
        expect(maybeJobStatus.getError()).toStrictEqual(new Error('Unexpected while getting job status, code: 200'))

        expect(sendMock).toHaveBeenCalledTimes(1)
        expect(sendMock.mock.calls[0][0].input).toStrictEqual({
            jobs: ['0001']
        })
    })
})
