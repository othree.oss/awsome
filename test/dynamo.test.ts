import {dynamo} from '../src/'
import {
    BatchGetItemCommand,
    DeleteItemCommand,
    PutItemCommand,
    QueryCommand,
    ScanCommand
} from '@aws-sdk/client-dynamodb'
import {marshall} from '@aws-sdk/util-dynamodb'
import {Optional} from '@othree.io/optional'
import {getBatchBy} from '../src/dynamo'

describe('dynamo', () => {
    const configuration: dynamo.DynamoConfiguration = {
        TableName: 'TheTable'
    }

    describe('upsert', () => {
        it('should upsert the specified entity', async () => {
            const dynamoDb = {
                send: jest.fn().mockReturnValue(Promise.resolve(true))
            }

            const upsert = dynamo.upsert(dynamoDb as any, configuration)

            const entity = {
                id: '1337',
                salutation: 'Hello world!'
            }

            const maybeEntity = await upsert(entity)

            expect(maybeEntity.isPresent).toBeTruthy()
            expect(maybeEntity.get()).toStrictEqual(entity)

            const sendMock = dynamoDb.send.mock
            expect(sendMock.calls.length).toEqual(1)
            expect(sendMock.calls[0][0] instanceof PutItemCommand).toBeTruthy()
            expect(sendMock.calls[0][0].input).toStrictEqual({
                Item: marshall(entity),
                TableName: configuration.TableName
            })
        })
    })

    describe('query', () => {
        it('should query based on the provided keys and index', async () => {
            const dynamoDb = {
                send: jest.fn().mockReturnValue(Promise.resolve({
                    Items: [
                        marshall({
                            id: '1337',
                            date: 9000,
                            salutation: 'Hello World!'
                        }),
                        marshall({
                            id: '1337',
                            date: 9000,
                            salutation: 'C ya'
                        })
                    ]
                }))
            }

            const query = dynamo.query(dynamoDb as any, {...configuration, IndexName: 'TableIdx'})

            const maybeResult = await query({id: '1337', date: 9000})

            expect(maybeResult.isPresent).toBeTruthy()
            expect(maybeResult.get()).toStrictEqual([
                {
                    id: '1337',
                    date: 9000,
                    salutation: 'Hello World!'
                },
                {
                    id: '1337',
                    date: 9000,
                    salutation: 'C ya'
                }
            ])

            const sendMock = dynamoDb.send.mock
            expect(sendMock.calls.length).toEqual(1)
            expect(sendMock.calls[0][0] instanceof QueryCommand).toBeTruthy()
            expect(sendMock.calls[0][0].input).toStrictEqual({
                TableName: configuration.TableName,
                IndexName: 'TableIdx',
                KeyConditionExpression: '#id = :id and #date = :date',
                FilterExpression: undefined,
                ExpressionAttributeNames: {
                    '#date': 'date',
                    '#id': 'id'
                },
                ExpressionAttributeValues: marshall({
                    ':id': '1337',
                    ':date': 9000
                }),
                ExclusiveStartKey: undefined
            })
        })

        it('should query based on the provided keys, index and filters', async () => {
            const dynamoDb = {
                send: jest.fn().mockReturnValue(Promise.resolve({
                    Items: [
                        marshall({
                            id: '1337',
                            date: 9000,
                            salutation: 'C ya'
                        })
                    ]
                }))
            }

            const query = dynamo.query(dynamoDb as any, {...configuration, IndexName: 'TableIdx'})

            const maybeResult = await query({id: '1337', date: 9000}, {salutation: 'C ya'})
            maybeResult.get()
            expect(maybeResult.isPresent).toBeTruthy()
            expect(maybeResult.get()).toStrictEqual([
                {
                    id: '1337',
                    date: 9000,
                    salutation: 'C ya'
                }
            ])

            const sendMock = dynamoDb.send.mock
            expect(sendMock.calls.length).toEqual(1)
            expect(sendMock.calls[0][0] instanceof QueryCommand).toBeTruthy()
            expect(sendMock.calls[0][0].input).toStrictEqual({
                TableName: configuration.TableName,
                IndexName: 'TableIdx',
                KeyConditionExpression: '#id = :id and #date = :date',
                FilterExpression: '#salutation = :salutation',
                ExpressionAttributeNames: {
                    '#date': 'date',
                    '#id': 'id',
                    '#salutation': 'salutation'
                },
                ExpressionAttributeValues: marshall({
                    ':id': '1337',
                    ':date': 9000,
                    ':salutation': 'C ya'
                }),
                ExclusiveStartKey: undefined
            })
        })

        it('should query based on the provided keys', async () => {
            const dynamoDb = {
                send: jest.fn().mockReturnValue(Promise.resolve({
                    Items: [
                        marshall({
                            id: '1337',
                            date: 9000,
                            salutation: 'Hello World!'
                        }),
                        marshall({
                            id: '1337',
                            date: 9000,
                            salutation: 'C ya'
                        })
                    ]
                }))
            }

            const query = dynamo.query(dynamoDb as any, configuration)

            const maybeResult = await query({id: '1337', date: 9000})

            expect(maybeResult.isPresent).toBeTruthy()
            expect(maybeResult.get()).toStrictEqual([
                {
                    id: '1337',
                    date: 9000,
                    salutation: 'Hello World!'
                },
                {
                    id: '1337',
                    date: 9000,
                    salutation: 'C ya'
                }
            ])

            const sendMock = dynamoDb.send.mock
            expect(sendMock.calls.length).toEqual(1)
            expect(sendMock.calls[0][0] instanceof QueryCommand).toBeTruthy()
            expect(sendMock.calls[0][0].input).toStrictEqual({
                TableName: configuration.TableName,
                IndexName: undefined,
                KeyConditionExpression: '#id = :id and #date = :date',
                FilterExpression: undefined,
                ExpressionAttributeNames: {
                    '#date': 'date',
                    '#id': 'id'
                },
                ExpressionAttributeValues: marshall({
                    ':id': '1337',
                    ':date': 9000
                }),
                ExclusiveStartKey: undefined
            })
        })

        it('should return an empty array if no items are returned', async () => {
            const dynamoDb = {
                send: jest.fn().mockReturnValue(Promise.resolve({}))
            }

            const query = dynamo.query(dynamoDb as any, configuration)

            const maybeResult = await query({id: '1337', date: 9000})

            expect(maybeResult.isPresent).toBeTruthy()
            expect(maybeResult.get()).toStrictEqual([])
        })

        it('should automatically paginate through the results', (async () => {
            const dynamoDb = {
                send: jest.fn().mockReturnValueOnce(Promise.resolve({
                    Items: [
                        marshall({
                            id: '1337',
                            date: 9000,
                            salutation: 'Hello World!'
                        }),
                        marshall({
                            id: '1337',
                            date: 9000,
                            salutation: 'C ya'
                        })
                    ],
                    LastEvaluatedKey: marshall({
                        id: '1337'
                    })
                })).mockReturnValueOnce(Promise.resolve({
                    Items: [
                        marshall({
                            id: '1234',
                            date: 1000,
                            salutation: 'Hola Mundo!'
                        }),
                        marshall({
                            id: '1234',
                            date: 1000,
                            salutation: 'Pura Vida'
                        })
                    ]
                }))
            }

            const query = dynamo.query(dynamoDb as any, {...configuration, IndexName: 'TableIdx'})

            const maybeResult = await query({id: '1337', date: 9000})

            expect(maybeResult.isPresent).toBeTruthy()
            expect(maybeResult.get()).toStrictEqual([
                {
                    id: '1337',
                    date: 9000,
                    salutation: 'Hello World!'
                },
                {
                    id: '1337',
                    date: 9000,
                    salutation: 'C ya'
                },
                {
                    id: '1234',
                    date: 1000,
                    salutation: 'Hola Mundo!'
                },
                {
                    id: '1234',
                    date: 1000,
                    salutation: 'Pura Vida'
                }
            ])

            const sendMock = dynamoDb.send.mock
            expect(sendMock.calls.length).toEqual(2)
            expect(sendMock.calls[0][0] instanceof QueryCommand).toBeTruthy()
            expect(sendMock.calls[0][0].input).toStrictEqual({
                TableName: configuration.TableName,
                IndexName: 'TableIdx',
                KeyConditionExpression: '#id = :id and #date = :date',
                FilterExpression: undefined,
                ExpressionAttributeNames: {
                    '#date': 'date',
                    '#id': 'id'
                },
                ExpressionAttributeValues: marshall({
                    ':id': '1337',
                    ':date': 9000
                }),
                ExclusiveStartKey: undefined
            })
            expect(sendMock.calls[1][0] instanceof QueryCommand).toBeTruthy()
            expect(sendMock.calls[1][0].input).toStrictEqual({
                TableName: configuration.TableName,
                IndexName: 'TableIdx',
                KeyConditionExpression: '#id = :id and #date = :date',
                FilterExpression: undefined,
                ExpressionAttributeNames: {
                    '#date': 'date',
                    '#id': 'id'
                },
                ExpressionAttributeValues: marshall({
                    ':id': '1337',
                    ':date': 9000
                }),
                ExclusiveStartKey: marshall({
                    'id': '1337'
                })
            })
        }))

        it('should try to query base on the provided keys return and return an empty optional with an error if the keys exceeded the primary and sort key', async () => {
            const dynamoDb = {
                send: jest.fn()
            }

            const query = dynamo.query(dynamoDb as any, configuration)

            const maybeResult = await query({id: '1337', date: 9000, salutation: 'Hello world!'})

            expect(maybeResult.isEmpty).toBeTruthy()
            expect(maybeResult.getError()).toStrictEqual(new Error('Only partition key and sort key are allowed to be query on, if you need more, try filtering'))

            expect(dynamoDb.send).toHaveBeenCalledTimes(0)
        })
    })

    describe('getBy', () => {
        it('should query based on the provided keys', async () => {
            const query = jest.fn().mockResolvedValue(Optional([
                    {
                        id: '1337',
                        date: 9000,
                        salutation: 'Hello World!'
                    },
                    {
                        id: '1337',
                        date: 9000,
                        salutation: 'C ya'
                    }
                ])
            )

            const getBy = dynamo.getBy(query)

            const maybeResult = await getBy({id: '1337', date: 9000})

            expect(maybeResult.isPresent).toBeTruthy()
            expect(maybeResult.get()).toStrictEqual([
                {
                    id: '1337',
                    date: 9000,
                    salutation: 'Hello World!'
                },
                {
                    id: '1337',
                    date: 9000,
                    salutation: 'C ya'
                }
            ])

            expect(query).toHaveBeenCalledTimes(1)
            expect(query).toHaveBeenCalledWith({id: '1337', date: 9000})
        })
    })

    describe('filterBy', () => {
        it('should filter based on the provided keys and filters', async () => {
            const query = jest.fn().mockResolvedValue(Optional([
                    {
                        id: '1337',
                        date: 9000,
                        salutation: 'C ya'
                    }
                ])
            )

            const filterBy = dynamo.filterBy(query)

            const maybeResult = await filterBy({id: '1337', date: 9000}, {salutation: 'C ya'})

            expect(maybeResult.isPresent).toBeTruthy()
            expect(maybeResult.get()).toStrictEqual([
                {
                    id: '1337',
                    date: 9000,
                    salutation: 'C ya'
                }
            ])

            expect(query).toHaveBeenCalledTimes(1)
            expect(query).toHaveBeenCalledWith({id: '1337', date: 9000}, {salutation: 'C ya'})
        })
    })

    describe('getBatchBy', () => {
        it('should get the requested items', async () => {
            const dynamoDb = {
                send: jest.fn().mockReturnValue(Promise.resolve({
                    Responses: {
                        'TheTable': [
                            marshall({
                                id: '1337',
                                date: 9000,
                                salutation: 'Hello World!'
                            }),
                            marshall({
                                id: '7113',
                                date: 9000,
                                salutation: 'C ya'
                            })
                        ]
                    }
                }))
            }

            const maybeResults = await getBatchBy(dynamoDb as any, configuration)([
                {
                    id: '1337'
                },
                {
                    id: '7331'
                }
            ])

            expect(maybeResults.isPresent).toBeTruthy()
            expect(maybeResults.get()).toStrictEqual([
                {
                    id: '1337',
                    date: 9000,
                    salutation: 'Hello World!'
                },
                {
                    id: '7113',
                    date: 9000,
                    salutation: 'C ya'
                }
            ])

            expect(dynamoDb.send).toBeCalledTimes(1)
            expect(dynamoDb.send.mock.calls[0][0] instanceof BatchGetItemCommand).toBeTruthy()
            expect(dynamoDb.send.mock.calls[0][0].input).toStrictEqual({
                RequestItems: {
                    TheTable: {
                        Keys: [
                            marshall({'id': '1337'}),
                            marshall({'id': '7331'})
                        ]
                    }
                }
            })
        })

        it('should return an empty array if no items are requested', async () => {
            const dynamoDb = {
                send: jest.fn()
            }

            const maybeResults = await getBatchBy(dynamoDb as any, configuration)([])

            expect(maybeResults.isPresent).toBeTruthy()
            expect(maybeResults.get()).toStrictEqual([])

            expect(dynamoDb.send).toBeCalledTimes(0)
        })

        it('should return an empty array if no items are returned', async () => {
            const dynamoDb = {
                send: jest.fn().mockReturnValue(Promise.resolve({}))
            }

            const maybeResults = await getBatchBy(dynamoDb as any, configuration)([
                {
                    id: '1337'
                },
                {
                    id: '7331'
                }
            ])

            expect(maybeResults.isPresent).toBeTruthy()
            expect(maybeResults.get()).toStrictEqual([])

            expect(dynamoDb.send).toBeCalledTimes(1)
            expect(dynamoDb.send.mock.calls[0][0] instanceof BatchGetItemCommand).toBeTruthy()
            expect(dynamoDb.send.mock.calls[0][0].input).toStrictEqual({
                RequestItems: {
                    TheTable: {
                        Keys: [
                            marshall({'id': '1337'}),
                            marshall({'id': '7331'})
                        ]
                    }
                }
            })
        })

        it('should automatically paginate through the results', (async () => {
            const dynamoDb = {
                send: jest.fn().mockReturnValueOnce(Promise.resolve({
                    Responses: {
                        'TheTable': [
                            marshall({
                                id: '1337',
                                date: 9000,
                                salutation: 'Hello World!'
                            })
                        ]
                    },
                    UnprocessedKeys: {
                        TheTable: {
                            Keys: [
                                marshall({'id': '7331'})
                            ]
                        }
                    }
                }))
                    .mockReturnValueOnce(Promise.resolve({
                        Responses: {
                            'TheTable': [
                                marshall({
                                    id: '7113',
                                    date: 9000,
                                    salutation: 'C ya'
                                })
                            ]
                        }
                    }))
            }

            const maybeResults = await getBatchBy(dynamoDb as any, configuration)([
                {
                    id: '1337'
                },
                {
                    id: '7331'
                }
            ])

            expect(maybeResults.isPresent).toBeTruthy()
            expect(maybeResults.get()).toStrictEqual([
                {
                    id: '1337',
                    date: 9000,
                    salutation: 'Hello World!'
                },
                {
                    id: '7113',
                    date: 9000,
                    salutation: 'C ya'
                }
            ])

            expect(dynamoDb.send).toBeCalledTimes(2)
            expect(dynamoDb.send.mock.calls[0][0] instanceof BatchGetItemCommand).toBeTruthy()
            expect(dynamoDb.send.mock.calls[0][0].input).toStrictEqual({
                RequestItems: {
                    TheTable: {
                        Keys: [
                            marshall({'id': '1337'}),
                            marshall({'id': '7331'})
                        ]
                    }
                }
            })
            expect(dynamoDb.send.mock.calls[1][0] instanceof BatchGetItemCommand).toBeTruthy()
            expect(dynamoDb.send.mock.calls[1][0].input).toStrictEqual({
                RequestItems: {
                    TheTable: {
                        Keys: [
                            marshall({'id': '7331'})
                        ]
                    }
                }
            })
        }))
    })

    describe('deleteBy', () => {
        it('should delete based on the provided keys', async () => {
            const dynamoDb = {
                send: jest.fn().mockReturnValue(Promise.resolve(true))
            }

            const deleteBy = dynamo.deleteBy(dynamoDb as any, configuration)

            const maybeResult = await deleteBy({id: '1337', date: 9000})

            expect(maybeResult.isPresent).toBeTruthy()
            expect(maybeResult.get()).toBeTruthy()

            const sendMock = dynamoDb.send.mock
            expect(sendMock.calls.length).toEqual(1)
            expect(sendMock.calls[0][0] instanceof DeleteItemCommand).toBeTruthy()
            expect(sendMock.calls[0][0].input).toStrictEqual({
                TableName: configuration.TableName,
                Key: marshall({
                    'id': '1337',
                    'date': 9000
                })
            })
        })
    })

    describe('getAll', () => {

        it('should scan with no provided keys', async () => {
            const dynamoDb = {
                send: jest.fn().mockReturnValue(Promise.resolve({
                    Items: [
                        marshall({
                            id: '1337',
                            date: 9000,
                            salutation: 'Hello World!'
                        }),
                        marshall({
                            id: '1337',
                            date: 9000,
                            salutation: 'C ya'
                        })
                    ]
                }))
            }

            const getAll = dynamo.getAll(dynamoDb as any, configuration)

            const maybeResult = await getAll()

            expect(maybeResult.isPresent).toBeTruthy()
            expect(maybeResult.get()).toStrictEqual([
                {
                    id: '1337',
                    date: 9000,
                    salutation: 'Hello World!'
                },
                {
                    id: '1337',
                    date: 9000,
                    salutation: 'C ya'
                }
            ])

            const sendMock = dynamoDb.send.mock
            expect(sendMock.calls.length).toEqual(1)
            expect(sendMock.calls[0][0] instanceof ScanCommand).toBeTruthy()
            expect(sendMock.calls[0][0].input).toStrictEqual({
                TableName: configuration.TableName,
                IndexName: undefined,
                ExclusiveStartKey: undefined
            })
        })

        it('should scan based on the provided keys and index', async () => {
            const dynamoDb = {
                send: jest.fn().mockReturnValue(Promise.resolve({
                    Items: [
                        marshall({
                            id: '1337',
                            date: 9000,
                            salutation: 'Hello World!'
                        }),
                        marshall({
                            id: '1337',
                            date: 9000,
                            salutation: 'C ya'
                        })
                    ]
                }))
            }

            const getAll = dynamo.getAll(dynamoDb as any, {...configuration, IndexName: 'TableIdx'})

            const maybeResult = await getAll()

            expect(maybeResult.isPresent).toBeTruthy()
            expect(maybeResult.get()).toStrictEqual([
                {
                    id: '1337',
                    date: 9000,
                    salutation: 'Hello World!'
                },
                {
                    id: '1337',
                    date: 9000,
                    salutation: 'C ya'
                }
            ])

            const sendMock = dynamoDb.send.mock
            expect(sendMock.calls.length).toEqual(1)
            expect(sendMock.calls[0][0] instanceof ScanCommand).toBeTruthy()
            expect(sendMock.calls[0][0].input).toStrictEqual({
                TableName: configuration.TableName,
                IndexName: 'TableIdx',
                ExclusiveStartKey: undefined
            })
        })

        it('should return an empty array if no items are returned', async () => {
            const dynamoDb = {
                send: jest.fn().mockReturnValue(Promise.resolve({}))
            }

            const getAll = dynamo.getAll(dynamoDb as any, configuration)

            const maybeResult = await getAll()

            expect(maybeResult.isPresent).toBeTruthy()
            expect(maybeResult.get()).toStrictEqual([])
        })

        it('should automatically paginate through the results', (async () => {
            const dynamoDb = {
                send: jest.fn().mockReturnValueOnce(Promise.resolve({
                    Items: [
                        marshall({
                            id: '1337',
                            date: 9000,
                            salutation: 'Hello World!'
                        }),
                        marshall({
                            id: '1337',
                            date: 9000,
                            salutation: 'C ya'
                        })
                    ],
                    LastEvaluatedKey: marshall({
                        id: '1337'
                    })
                })).mockReturnValueOnce(Promise.resolve({
                    Items: [
                        marshall({
                            id: '1234',
                            date: 1000,
                            salutation: 'Hola Mundo!'
                        }),
                        marshall({
                            id: '1234',
                            date: 1000,
                            salutation: 'Pura Vida'
                        })
                    ]
                }))
            }

            const getAll = dynamo.getAll(dynamoDb as any, {...configuration, IndexName: 'TableIdx'})

            const maybeResult = await getAll()

            expect(maybeResult.isPresent).toBeTruthy()
            expect(maybeResult.get()).toStrictEqual([
                {
                    id: '1337',
                    date: 9000,
                    salutation: 'Hello World!'
                },
                {
                    id: '1337',
                    date: 9000,
                    salutation: 'C ya'
                },
                {
                    id: '1234',
                    date: 1000,
                    salutation: 'Hola Mundo!'
                },
                {
                    id: '1234',
                    date: 1000,
                    salutation: 'Pura Vida'
                }
            ])

            const sendMock = dynamoDb.send.mock
            expect(sendMock.calls.length).toEqual(2)
            expect(sendMock.calls[0][0] instanceof ScanCommand).toBeTruthy()
            expect(sendMock.calls[0][0].input).toStrictEqual({
                TableName: configuration.TableName,
                IndexName: 'TableIdx',
                ExclusiveStartKey: undefined
            })
            expect(sendMock.calls[1][0] instanceof ScanCommand).toBeTruthy()
            expect(sendMock.calls[1][0].input).toStrictEqual({
                TableName: configuration.TableName,
                IndexName: 'TableIdx',
                ExclusiveStartKey: marshall({
                    'id': '1337'
                })
            })
        }))
    })
})
