import {
    ChangeMessageVisibilityCommand,
    CreateQueueCommand,
    DeleteMessageBatchCommand,
    DeleteQueueCommand,
    GetQueueAttributesCommand,
    ReceiveMessageCommand,
    SendMessageBatchCommand,
    SendMessageCommand,
} from '@aws-sdk/client-sqs'
import {sqs} from '../src'
import {
    ChangeMessageVisibilityTimeoutRequest,
    getApproximateMessageCount, getQueueUrl,
    MessageCountRequest,
} from '../src/sqs'

describe('send', () => {

    const configuration: sqs.SQSConfiguration = {
        QueueUrl: 'https://sqs.us-west-2.amazonaws.com/12345/TestQueue',
    }

    type Payload = {
        readonly id: string
        readonly traceId: string
    }

    const payload1: Payload = {
        id: '0001',
        traceId: '0002',
    }

    it('should send a message', async () => {
        const sqsClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                MessageId: 'm355463',
                SequenceNumber: '1',
            })),
        }

        const maybeMessageSent = await sqs.send<Payload>(sqsClient as any, configuration)()()()()(payload1)

        expect(maybeMessageSent.isPresent).toBeTruthy()
        expect(maybeMessageSent.get()).toBeTruthy()

        const sendMock = sqsClient.send.mock
        expect(sendMock.calls.length).toEqual(1)
        expect(sendMock.calls[0][0] instanceof SendMessageCommand).toBeTruthy()
        expect(sendMock.calls[0][0].input).toStrictEqual({
            QueueUrl: configuration.QueueUrl,
            MessageBody: JSON.stringify(payload1),
            DelaySeconds: undefined,
            MessageGroupId: undefined,
            MessageDeduplicationId: undefined,
            MessageAttributes: undefined,
        })
    })

    it('should send a message using all available options', async () => {
        const sqsClient = {
            send: jest.fn().mockReturnValue(Promise.resolve(true)),
        }

        const maybeMessageSent = await sqs.send(sqsClient as any, configuration)
        ((payload: Payload) => payload.id)
        ((payload: Payload) => `${payload.id}-${payload.traceId}`)
        ((payload: Payload) => ({
            traceId: {
                DataType: 'String',
                StringValue: payload.id,
            },
        }))(2)(payload1)

        expect(maybeMessageSent.isPresent).toBeTruthy()
        expect(maybeMessageSent.get()).toBeTruthy()

        const sendMock = sqsClient.send.mock
        expect(sendMock.calls.length).toEqual(1)
        expect(sendMock.calls[0][0] instanceof SendMessageCommand).toBeTruthy()
        expect(sendMock.calls[0][0].input).toStrictEqual({
            QueueUrl: configuration.QueueUrl,
            MessageBody: JSON.stringify(payload1),
            DelaySeconds: 2,
            MessageGroupId: '0001',
            MessageDeduplicationId: '0001-0002',
            MessageAttributes: {
                traceId: {
                    DataType: 'String',
                    StringValue: '0001',
                },
            },
        })
    })
})

describe('sendBatch', () => {
    const configuration: sqs.SQSConfiguration = {
        QueueUrl: 'https://sqs.us-west-2.amazonaws.com/12345/TestQueue',
    }

    type Payload = {
        readonly id: string
        readonly traceId: string
    }

    const payload1: Payload = {
        id: '0001',
        traceId: '0002',
    }

    const payload2: Payload = {
        id: '0002',
        traceId: '0003',
    }

    it('should send the messages', async () => {
        const sqsClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                Successful: [{Id: '0001'}],
                Failed: [{Id: '0002'}],
            })),
        }

        const maybeMessageSent = await sqs.sendBatch<Payload>(sqsClient as any, configuration)(_ => _.id)()()()()([payload1, payload2])

        expect(maybeMessageSent.isPresent).toBeTruthy()
        expect(maybeMessageSent.get()).toStrictEqual({
            successful: ['0001'],
            failures: ['0002'],
        })

        const sendMock = sqsClient.send.mock
        expect(sendMock.calls.length).toEqual(1)
        expect(sendMock.calls[0][0] instanceof SendMessageBatchCommand).toBeTruthy()
        expect(sendMock.calls[0][0].input).toStrictEqual({
            QueueUrl: configuration.QueueUrl,
            Entries: [
                {
                    Id: '0001',
                    MessageBody: JSON.stringify(payload1),
                    DelaySeconds: undefined,
                    MessageGroupId: undefined,
                    MessageDeduplicationId: undefined,
                    MessageAttributes: undefined,
                },
                {
                    Id: '0002',
                    MessageBody: JSON.stringify(payload2),
                    DelaySeconds: undefined,
                    MessageGroupId: undefined,
                    MessageDeduplicationId: undefined,
                    MessageAttributes: undefined,
                },
            ],
        })
    })

    it('should send the messages using all available options', async () => {
        const sqsClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                Successful: [{Id: '0001'}],
                Failed: [{Id: '0002'}],
            })),
        }

        const maybeMessageSent = await sqs.sendBatch(sqsClient as any, configuration)
        (_ => _.id)
        ((payload: Payload) => payload.id)
        ((payload: Payload) => `${payload.id}-${payload.traceId}`)
        ((payload: Payload) => ({
            traceId: {
                DataType: 'String',
                StringValue: payload.id,
            },
        }))(2)([payload1, payload2])

        expect(maybeMessageSent.isPresent).toBeTruthy()
        expect(maybeMessageSent.get()).toStrictEqual({
            successful: ['0001'],
            failures: ['0002'],
        })

        const sendMock = sqsClient.send.mock
        expect(sendMock.calls.length).toEqual(1)
        expect(sendMock.calls[0][0].input).toStrictEqual({
            QueueUrl: configuration.QueueUrl,
            Entries: [
                {
                    Id: '0001',
                    MessageBody: JSON.stringify(payload1),
                    DelaySeconds: 2,
                    MessageGroupId: '0001',
                    MessageDeduplicationId: '0001-0002',
                    MessageAttributes: {
                        traceId: {
                            DataType: 'String',
                            StringValue: '0001',
                        },
                    },
                },
                {
                    Id: '0002',
                    MessageBody: JSON.stringify(payload2),
                    DelaySeconds: 2,
                    MessageGroupId: '0002',
                    MessageDeduplicationId: '0002-0003',
                    MessageAttributes: {
                        traceId: {
                            DataType: 'String',
                            StringValue: '0002',
                        },
                    },
                },
            ],
        })
    })
})

describe('createQueue', () => {
    it('should create a new queue', async () => {
        const sqsClient = {
            send: jest.fn().mockReturnValueOnce(Promise.resolve({
                QueueUrl: 'https://sqs.us-west-2.amazonaws.com/12345/TestQueue',
            }))
                .mockReturnValueOnce(Promise.resolve({
                    Attributes: {
                        QueueArn: 'arn:aws:sqs:us-west-2:12345:TestQueue',
                    },
                })),
        }

        const maybeQueue = await sqs.createQueue(sqsClient as any)({
            queueName: 'TestQueue',
        })

        expect(maybeQueue.isPresent).toBeTruthy()
        expect(maybeQueue.get()).toStrictEqual({
            url: 'https://sqs.us-west-2.amazonaws.com/12345/TestQueue',
            arn: 'arn:aws:sqs:us-west-2:12345:TestQueue',
        })

        expect(sqsClient.send).toBeCalledTimes(2)
        expect(sqsClient.send.mock.calls[0][0] instanceof CreateQueueCommand).toBeTruthy()
        expect(sqsClient.send.mock.calls[0][0].input).toStrictEqual({
            QueueName: 'TestQueue',
        })

        expect(sqsClient.send.mock.calls[1][0] instanceof GetQueueAttributesCommand).toBeTruthy()
        expect(sqsClient.send.mock.calls[1][0].input).toStrictEqual({
            QueueUrl: 'https://sqs.us-west-2.amazonaws.com/12345/TestQueue',
            AttributeNames: ['QueueArn'],
        })
    })

    it('should create a new fifo queue', async () => {
        const sqsClient = {
            send: jest.fn().mockReturnValueOnce(Promise.resolve({
                QueueUrl: 'https://sqs.us-west-2.amazonaws.com/12345/TestQueue.fifo',
            }))
                .mockReturnValueOnce(Promise.resolve({
                    Attributes: {
                        QueueArn: 'arn:aws:sqs:us-west-2:12345:TestQueue.fifo',
                    },
                })),
        }

        const maybeQueue = await sqs.createQueue(sqsClient as any)({
            queueName: 'TestQueue.fifo',
            fifo: {
                contentBasedDeduplication: true,
                deduplicationScope: 'messageGroup',
                throughputLimit: 'perMessageGroupId',
            },
        })

        expect(maybeQueue.isPresent).toBeTruthy()
        expect(maybeQueue.get()).toStrictEqual({
            url: 'https://sqs.us-west-2.amazonaws.com/12345/TestQueue.fifo',
            arn: 'arn:aws:sqs:us-west-2:12345:TestQueue.fifo',
        })

        expect(sqsClient.send).toBeCalledTimes(2)
        expect(sqsClient.send.mock.calls[0][0] instanceof CreateQueueCommand).toBeTruthy()
        expect(sqsClient.send.mock.calls[0][0].input).toStrictEqual({
            QueueName: 'TestQueue.fifo',
            Attributes: {
                FifoQueue: 'true',
                ContentBasedDeduplication: 'true',
                DeduplicationScope: 'messageGroup',
                FifoThroughputLimit: 'perMessageGroupId',
            },
        })

        expect(sqsClient.send.mock.calls[1][0] instanceof GetQueueAttributesCommand).toBeTruthy()
        expect(sqsClient.send.mock.calls[1][0].input).toStrictEqual({
            QueueUrl: 'https://sqs.us-west-2.amazonaws.com/12345/TestQueue.fifo',
            AttributeNames: ['QueueArn'],
        })
    })
})

describe('deleteQueue', () => {
    it('should create the queue', async () => {
        const sqsClient = {
            send: jest.fn().mockReturnValueOnce(Promise.resolve({})),
        }

        const maybeQueue = await sqs.deleteQueue(sqsClient as any)({
            queueUrl: 'https://sqs.us-west-2.amazonaws.com/12345/TestQueue',
        })

        expect(maybeQueue.isPresent).toBeTruthy()
        expect(maybeQueue.get()).toBeTruthy()

        expect(sqsClient.send).toBeCalledTimes(1)
        expect(sqsClient.send.mock.calls[0][0] instanceof DeleteQueueCommand).toBeTruthy()
        expect(sqsClient.send.mock.calls[0][0].input).toStrictEqual({
            QueueUrl: 'https://sqs.us-west-2.amazonaws.com/12345/TestQueue',
        })
    })
})

describe('receiveMessages', () => {
    const configuration: sqs.ReceiveMessagesConfiguration = {
        QueueUrl: 'https://sqs.us-west-2.amazonaws.com/12345/TestQueue',
        maxNumberOfMessages: 10,
        waitTimeSeconds: 20,
        autoAcknowledgeMessages: false,
    }
    it('should receive messages from the queue', async () => {
        const sqsClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                Messages: [{'something': 'cool'}],
            })),
        }

        const transformMessage = jest.fn().mockReturnValue({'cool': 'something'})

        const maybeMessages = await sqs.receiveMessages(sqsClient as any, configuration)(transformMessage)()

        expect(maybeMessages.isPresent).toBeTruthy()
        expect(maybeMessages.get()).toStrictEqual([{'cool': 'something'}])

        expect(transformMessage).toBeCalledTimes(1)
        expect(transformMessage).toBeCalledWith({'something': 'cool'})

        expect(sqsClient.send).toBeCalledTimes(1)
        expect(sqsClient.send.mock.calls[0][0] instanceof ReceiveMessageCommand).toBeTruthy()
        expect(sqsClient.send.mock.calls[0][0].input).toStrictEqual({
            QueueUrl: configuration.QueueUrl,
            MaxNumberOfMessages: configuration.maxNumberOfMessages,
            AttributeNames: ['All'],
            WaitTimeSeconds: configuration.waitTimeSeconds,
        })
    })

    it('should receive and delete messages from the queue', async () => {
        const sqsClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                Messages: [{MessageId: '0001', ReceiptHandle: 'r3c31p7'}],
            })),
        }

        const transformMessage = jest.fn().mockReturnValue({'cool': 'something'})

        const maybeMessages = await sqs.receiveMessages(sqsClient as any, {
            ...configuration,
            autoAcknowledgeMessages: true,
        })(transformMessage)()

        expect(maybeMessages.isPresent).toBeTruthy()
        expect(maybeMessages.get()).toStrictEqual([{'cool': 'something'}])

        expect(transformMessage).toBeCalledTimes(1)
        expect(transformMessage).toBeCalledWith({MessageId: '0001', ReceiptHandle: 'r3c31p7'})

        expect(sqsClient.send).toBeCalledTimes(2)
        expect(sqsClient.send.mock.calls[0][0] instanceof ReceiveMessageCommand).toBeTruthy()
        expect(sqsClient.send.mock.calls[0][0].input).toStrictEqual({
            QueueUrl: configuration.QueueUrl,
            MaxNumberOfMessages: configuration.maxNumberOfMessages,
            AttributeNames: ['All'],
            WaitTimeSeconds: configuration.waitTimeSeconds,
        })
        expect(sqsClient.send.mock.calls[1][0] instanceof DeleteMessageBatchCommand).toBeTruthy()
        expect(sqsClient.send.mock.calls[1][0].input).toStrictEqual({
            QueueUrl: configuration.QueueUrl,
            Entries: [
                {
                    Id: '0001',
                    ReceiptHandle: 'r3c31p7',
                },
            ],
        })
    })

    it('should try to receive messages from the queue and return an empty array if there are none', async () => {
        const sqsClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                Messages: undefined,
            })),
        }

        const transformMessage = jest.fn()

        const maybeMessages = await sqs.receiveMessages(sqsClient as any, configuration)(transformMessage)()

        expect(maybeMessages.isPresent).toBeTruthy()
        expect(maybeMessages.get()).toStrictEqual([])

        expect(transformMessage).toBeCalledTimes(0)

        expect(sqsClient.send).toBeCalledTimes(1)
        expect(sqsClient.send.mock.calls[0][0] instanceof ReceiveMessageCommand).toBeTruthy()
        expect(sqsClient.send.mock.calls[0][0].input).toStrictEqual({
            QueueUrl: configuration.QueueUrl,
            MaxNumberOfMessages: configuration.maxNumberOfMessages,
            AttributeNames: ['All'],
            WaitTimeSeconds: configuration.waitTimeSeconds,
        })
    })
})

describe('getApproximateMessageCount', () => {
    it('should get the approximate message count of a queue', async () => {
        const request: MessageCountRequest = {
            queueUrl: 'https://sqs.us-west-2.amazonaws.com/12345/TestQueue'
        }

        const sendMock = jest.fn().mockResolvedValue({
            $metadata: {
                httpStatusCode: 200
            },
            Attributes: {
                ApproximateNumberOfMessages: 1
            }
        })

        const client = {
            send: sendMock
        } as any

        const maybeApproximateNumberOfMessages = await getApproximateMessageCount(client)(request)

        expect(maybeApproximateNumberOfMessages.isPresent).toBeTruthy()
        expect(maybeApproximateNumberOfMessages.get()).toEqual(1)

        expect(sendMock).toHaveBeenCalledTimes(1)
        expect(sendMock.mock.calls[0][0].input).toStrictEqual({
            QueueUrl: 'https://sqs.us-west-2.amazonaws.com/12345/TestQueue'
        })
    })

    it('should try to get the approximate message count of a queue and return an empty optional if the status code is not 200', async () => {
        const request: MessageCountRequest = {
            queueUrl: 'https://sqs.us-west-2.amazonaws.com/12345/TestQueue'
        }

        const sendMock = jest.fn().mockResolvedValue({
            $metadata: {
                httpStatusCode: 500
            },
            Attributes: {
                ApproximateNumberOfMessages: 1
            }
        })

        const client = {
            send: sendMock
        } as any

        const maybeApproximateNumberOfMessages = await getApproximateMessageCount(client)(request)

        expect(maybeApproximateNumberOfMessages.isEmpty).toBeTruthy()
        expect(maybeApproximateNumberOfMessages.getError()).toEqual(new Error('Cannot get queue attributes'))

        expect(sendMock).toHaveBeenCalledTimes(1)
        expect(sendMock.mock.calls[0][0].input).toStrictEqual({
            QueueUrl: 'https://sqs.us-west-2.amazonaws.com/12345/TestQueue'
        })
    })
})

describe('changeMessageVisibilityTimeout', () => {
    it('should change the visibility timeout', async () => {
        const sqsClient = {
            send: jest.fn().mockResolvedValueOnce({})
        }

        const request: ChangeMessageVisibilityTimeoutRequest = {
            timeout: 10,
            receiptHandle: 'h4ndl3',
            queue: 'https://sqs.us-west-2.amazonaws.com/12345/TestQueue'
        }

        const maybeChanged = await sqs.changeMessageVisibilityTimeout(sqsClient as any)(request)

        expect(maybeChanged.isPresent).toBeTruthy()
        expect(maybeChanged.get()).toBeTruthy()

        expect(sqsClient.send).toBeCalledTimes(1)
        expect(sqsClient.send.mock.calls[0][0] instanceof ChangeMessageVisibilityCommand).toBeTruthy()
        expect(sqsClient.send.mock.calls[0][0].input).toStrictEqual({
            ReceiptHandle: request.receiptHandle,
            QueueUrl: request.queue,
            VisibilityTimeout: request.timeout,
        })
    })
})

describe('getQueueUrl', () => {
    it('should get the queue url', () => {
        const url = getQueueUrl({
            queueArn: 'arn:aws:sqs:us-west-2:4cc0un7:Cocoa-TheQueue.fifo'
        })

        expect(url).toEqual({
            queueUrl: 'https://sqs.us-west-2.amazonaws.com/4cc0un7/Cocoa-TheQueue.fifo'
        })
    })
})
