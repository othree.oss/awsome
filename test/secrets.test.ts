import {secretsManager} from '../src'
import {GetSecretValueCommand, PutSecretValueCommand} from '@aws-sdk/client-secrets-manager'

const encoder = new TextEncoder()

describe('get', () => {

    it('should get the secret value when the secret is a string', async () => {
        const secretsManagerClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                SecretString: 's3cr3t'
            })),
            config: undefined as any,
            middlewareStack: undefined as any,
            destroy: () => undefined
        }

        const maybeSecretValue = await secretsManager.get(secretsManagerClient)('s3cr3t1d')

        expect(maybeSecretValue.isPresent).toBeTruthy()
        expect(maybeSecretValue.get()).toStrictEqual('s3cr3t')

        const sendMock = secretsManagerClient.send.mock
        expect(sendMock.calls.length).toEqual(1)
        expect(sendMock.calls[0][0] instanceof GetSecretValueCommand).toBeTruthy()
        expect(sendMock.calls[0][0].input).toStrictEqual({
            SecretId: 's3cr3t1d'
        })
    })

    it('should get the secret value when the secret is encoded', async () => {
        const secretsManagerClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                SecretBinary: encoder.encode('s3cr3t')
            })),
            config: undefined as any,
            middlewareStack: undefined as any,
            destroy: () => undefined
        }

        const maybeSecretValue = await secretsManager.get(secretsManagerClient)('s3cr3t1d')

        expect(maybeSecretValue.isPresent).toBeTruthy()
        expect(maybeSecretValue.get()).toStrictEqual('s3cr3t')
    })
})

describe('put', () => {

    it('should put the secret value', async () => {

        const secretsManagerClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                $metadata: {
                    httpStatusCode: 200
                }
            })),
            config: undefined as any,
            middlewareStack: undefined as any,
            destroy: () => undefined
        }

        const input: Record<string, string> = {
            username: 'username',
            password: 's3cr3t'
        }
        const maybeResponse = await secretsManager.put(secretsManagerClient)('s3cr3t1d')(input)

        expect(maybeResponse.isPresent).toBeTruthy()
        expect(maybeResponse.get()).toBeTruthy()

        const sendMock = secretsManagerClient.send.mock
        expect(sendMock.calls.length).toEqual(1)
        expect(sendMock.calls[0][0] instanceof PutSecretValueCommand).toBeTruthy()
        expect(sendMock.calls[0][0].input).toStrictEqual({
            SecretId: 's3cr3t1d',
            SecretString: '{"username":"username","password":"s3cr3t"}'
        })
    })
})
