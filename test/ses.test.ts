import {ses} from '../src'
import {SendRawEmailCommand} from '@aws-sdk/client-ses'

describe('ses', () => {
    describe('sendMimeEmail', () => {
        const mimeEmail = 'Date: Fri, 07 Jan 2022 22:05:59 +0000\n' +
            '    From: "Foo Bar" <from@doterra.com>\n' +
            '    To: "Foo Bar" <to@doterra.com>\n' +
            '    Message-ID: <i5tq6ix3tlk-1641765959795@doterra.com>\n' +
            '    Subject: =?utf-8?B?IElzc3VlIDQ5IQ==?=\n' +
            '    MIME-Version: 1.0\n' +
            '    X-Abc: abc\n' +
            '    Content-Type: multipart/alternative; boundary=3w46v55n45t\n' +
            '    \n' +
            '    --3w46v55n45t\n' +
            '    Content-Type: text/plain; charset=UTF-8\n' +
            '    \n' +
            '    Hello,\n' +
            '    World.\n' +
            '    \n' +
            '    --3w46v55n45t'

        it('should send a formatted mime email', async () => {
            const sesClient = {
                send: jest.fn().mockReturnValue(Promise.resolve({
                    MessageId: '0001'
                }))
            }

            const maybeMailSent = await ses.sendMimeEmail(sesClient as any)(Buffer.from(mimeEmail))

            expect(maybeMailSent.isPresent).toBeTruthy()
            expect(maybeMailSent.get()).toEqual('0001')

            expect(sesClient.send.mock.calls.length).toEqual(1)
            expect(sesClient.send.mock.calls[0][0] instanceof SendRawEmailCommand).toBeTruthy()
            expect(sesClient.send.mock.calls[0][0].input).toStrictEqual({
                RawMessage: {
                    Data: Buffer.from(mimeEmail)
                }
            })
        })
    })

})
