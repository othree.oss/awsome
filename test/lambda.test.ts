import {InvocationError, invoke, query, command, request} from '../src/lambda'
import {InvocationType, InvokeCommand} from '@aws-sdk/client-lambda'

const encoder = new TextEncoder()

describe('invoke', () => {
    interface TestPayload {
        readonly salutation: string
    }

    const testPayload: TestPayload = {
        salutation: 'Hello World!'
    }

    const testPayloadResponse = {
        response: 'Hey there!'
    }

    const functionName = 'SomeBcCommandHandler'

    it('should invoke the lambda function', async () => {
        const lambdaClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                StatusCode: 200,
                Payload: encoder.encode(JSON.stringify(testPayloadResponse))
            }))
        }
        const maybeResponse = await invoke(lambdaClient as any, functionName)(testPayload)

        expect(maybeResponse.isPresent).toBeTruthy()
        expect(maybeResponse.get()).toStrictEqual({...testPayloadResponse})
        expect(lambdaClient.send.mock.calls.length).toEqual(1)
        expect(lambdaClient.send.mock.calls[0][0] instanceof InvokeCommand)
        expect(lambdaClient.send.mock.calls[0][0].input).toEqual({
            FunctionName: functionName,
            InvocationType: InvocationType.RequestResponse,
            Payload: encoder.encode(JSON.stringify(testPayload))
        })
    })

    it('should throw an invocation error if the status code is 200 and there is a function error', async () => {
        const lambdaClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                StatusCode: 200,
                FunctionError: 'error',
                Payload: encoder.encode(JSON.stringify({
                    errorType: 'NotFound',
                    errorMessage: 'Something was not found'
                }))
            }))
        }
        const maybeResponse = await invoke(lambdaClient as any, functionName)(testPayload)

        expect(maybeResponse.isEmpty).toBeTruthy()
        expect(maybeResponse.getError()).toStrictEqual(new InvocationError('NotFound', 'Something was not found'))
        expect(lambdaClient.send.mock.calls.length).toEqual(1)
        expect(lambdaClient.send.mock.calls[0][0] instanceof InvokeCommand)
        expect(lambdaClient.send.mock.calls[0][0].input).toEqual({
            FunctionName: functionName,
            InvocationType: InvocationType.RequestResponse,
            Payload: encoder.encode(JSON.stringify(testPayload))
        })
    })

    it('should throw an invocation error if the status code is not 500', async () => {
        const lambdaClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                StatusCode: 500
            }))
        }
        const maybeResponse = await invoke(lambdaClient as any, functionName)(testPayload)

        expect(maybeResponse.isEmpty).toBeTruthy()
        expect(maybeResponse.getError()).toStrictEqual(new InvocationError('InvocationStatusCode', 500))
        expect(lambdaClient.send.mock.calls.length).toEqual(1)
        expect(lambdaClient.send.mock.calls[0][0] instanceof InvokeCommand)
        expect(lambdaClient.send.mock.calls[0][0].input).toEqual({
            FunctionName: functionName,
            InvocationType: InvocationType.RequestResponse,
            Payload: encoder.encode(JSON.stringify(testPayload))
        })
    })

    it('should throw an exception if the functionName is undefined', () => {
        expect(() => invoke(jest.fn() as any, undefined!)).toThrow(new Error('functionName is required'))
    })
})

describe('query', () => {
    interface TestPayload {
        readonly salutation: string
    }

    const testPayload: TestPayload = {
        salutation: 'Hello World!'
    }

    const testPayloadResponse = {
        response: 'Hey there!'
    }

    const functionName = 'SomeBcCommandHandler'

    it('should invoke the lambda function', async () => {
        const lambdaClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                StatusCode: 200,
                Payload: encoder.encode(JSON.stringify(testPayloadResponse))
            }))
        }
        const maybeResponse = await query(lambdaClient as any, functionName)(testPayload)

        expect(maybeResponse.isPresent).toBeTruthy()
        expect(maybeResponse.get()).toStrictEqual({...testPayloadResponse})
        expect(lambdaClient.send.mock.calls.length).toEqual(1)
        expect(lambdaClient.send.mock.calls[0][0] instanceof InvokeCommand)
        expect(lambdaClient.send.mock.calls[0][0].input).toEqual({
            FunctionName: functionName,
            InvocationType: InvocationType.RequestResponse,
            Payload: encoder.encode(JSON.stringify({query: testPayload}))
        })
    })

    it('should throw an invocation error if the status code is 200 and there is a function error', async () => {
        const lambdaClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                StatusCode: 200,
                FunctionError: 'error',
                Payload: encoder.encode(JSON.stringify({
                    errorType: 'NotFound',
                    errorMessage: 'Something was not found'
                }))
            }))
        }
        const maybeResponse = await query(lambdaClient as any, functionName)(testPayload)

        expect(maybeResponse.isEmpty).toBeTruthy()
        expect(maybeResponse.getError()).toStrictEqual(new InvocationError('NotFound', 'Something was not found'))
        expect(lambdaClient.send.mock.calls.length).toEqual(1)
        expect(lambdaClient.send.mock.calls[0][0] instanceof InvokeCommand)
        expect(lambdaClient.send.mock.calls[0][0].input).toEqual({
            FunctionName: functionName,
            InvocationType: InvocationType.RequestResponse,
            Payload: encoder.encode(JSON.stringify({query: testPayload}))
        })
    })

    it('should throw an invocation error if the status code is not 500', async () => {
        const lambdaClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                StatusCode: 500
            }))
        }
        const maybeResponse = await query(lambdaClient as any, functionName)(testPayload)

        expect(maybeResponse.isEmpty).toBeTruthy()
        expect(maybeResponse.getError()).toStrictEqual(new InvocationError('InvocationStatusCode', 500))
        expect(lambdaClient.send.mock.calls.length).toEqual(1)
        expect(lambdaClient.send.mock.calls[0][0] instanceof InvokeCommand)
        expect(lambdaClient.send.mock.calls[0][0].input).toEqual({
            FunctionName: functionName,
            InvocationType: InvocationType.RequestResponse,
            Payload: encoder.encode(JSON.stringify({query: testPayload}))
        })
    })
})

describe('command', () => {
    interface TestPayload {
        readonly salutation: string
    }

    const testPayload: TestPayload = {
        salutation: 'Hello World!'
    }

    const testPayloadResponse = {
        response: 'Hey there!'
    }

    const functionName = 'SomeBcCommandHandler'

    it('should invoke the lambda function', async () => {
        const lambdaClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                StatusCode: 200,
                Payload: encoder.encode(JSON.stringify(testPayloadResponse))
            }))
        }
        const maybeResponse = await command(lambdaClient as any, functionName)(testPayload)

        expect(maybeResponse.isPresent).toBeTruthy()
        expect(maybeResponse.get()).toStrictEqual({...testPayloadResponse})
        expect(lambdaClient.send.mock.calls.length).toEqual(1)
        expect(lambdaClient.send.mock.calls[0][0] instanceof InvokeCommand)
        expect(lambdaClient.send.mock.calls[0][0].input).toEqual({
            FunctionName: functionName,
            InvocationType: InvocationType.RequestResponse,
            Payload: encoder.encode(JSON.stringify({command: testPayload}))
        })
    })

    it('should throw an invocation error if the status code is 200 and there is a function error', async () => {
        const lambdaClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                StatusCode: 200,
                FunctionError: 'error',
                Payload: encoder.encode(JSON.stringify({
                    errorType: 'NotFound',
                    errorMessage: 'Something was not found'
                }))
            }))
        }
        const maybeResponse = await command(lambdaClient as any, functionName)(testPayload)

        expect(maybeResponse.isEmpty).toBeTruthy()
        expect(maybeResponse.getError()).toStrictEqual(new InvocationError('NotFound', 'Something was not found'))
        expect(lambdaClient.send.mock.calls.length).toEqual(1)
        expect(lambdaClient.send.mock.calls[0][0] instanceof InvokeCommand)
        expect(lambdaClient.send.mock.calls[0][0].input).toEqual({
            FunctionName: functionName,
            InvocationType: InvocationType.RequestResponse,
            Payload: encoder.encode(JSON.stringify({command: testPayload}))
        })
    })

    it('should throw an invocation error if the status code is not 500', async () => {
        const lambdaClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                StatusCode: 500
            }))
        }
        const maybeResponse = await command(lambdaClient as any, functionName)(testPayload)

        expect(maybeResponse.isEmpty).toBeTruthy()
        expect(maybeResponse.getError()).toStrictEqual(new InvocationError('InvocationStatusCode', 500))
        expect(lambdaClient.send.mock.calls.length).toEqual(1)
        expect(lambdaClient.send.mock.calls[0][0] instanceof InvokeCommand)
        expect(lambdaClient.send.mock.calls[0][0].input).toEqual({
            FunctionName: functionName,
            InvocationType: InvocationType.RequestResponse,
            Payload: encoder.encode(JSON.stringify({command: testPayload}))
        })
    })
})

describe('request', () => {
    interface TestPayload {
        readonly salutation: string
    }

    const testPayload: TestPayload = {
        salutation: 'Hello World!'
    }

    const testPayloadResponse = {
        response: 'Hey there!'
    }

    const functionName = 'SomeBcCommandHandler'

    it('should invoke the lambda function', async () => {
        const lambdaClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                StatusCode: 200,
                Payload: encoder.encode(JSON.stringify(testPayloadResponse))
            }))
        }
        const maybeResponse = await request(lambdaClient as any, functionName)(testPayload)

        expect(maybeResponse.isPresent).toBeTruthy()
        expect(maybeResponse.get()).toStrictEqual({...testPayloadResponse})
        expect(lambdaClient.send.mock.calls.length).toEqual(1)
        expect(lambdaClient.send.mock.calls[0][0] instanceof InvokeCommand)
        expect(lambdaClient.send.mock.calls[0][0].input).toEqual({
            FunctionName: functionName,
            InvocationType: InvocationType.RequestResponse,
            Payload: encoder.encode(JSON.stringify({request: testPayload}))
        })
    })

    it('should throw an invocation error if the status code is 200 and there is a function error', async () => {
        const lambdaClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                StatusCode: 200,
                FunctionError: 'error',
                Payload: encoder.encode(JSON.stringify({
                    errorType: 'NotFound',
                    errorMessage: 'Something was not found'
                }))
            }))
        }
        const maybeResponse = await request(lambdaClient as any, functionName)(testPayload)

        expect(maybeResponse.isEmpty).toBeTruthy()
        expect(maybeResponse.getError()).toStrictEqual(new InvocationError('NotFound', 'Something was not found'))
        expect(lambdaClient.send.mock.calls.length).toEqual(1)
        expect(lambdaClient.send.mock.calls[0][0] instanceof InvokeCommand)
        expect(lambdaClient.send.mock.calls[0][0].input).toEqual({
            FunctionName: functionName,
            InvocationType: InvocationType.RequestResponse,
            Payload: encoder.encode(JSON.stringify({request: testPayload}))
        })
    })

    it('should throw an invocation error if the status code is not 500', async () => {
        const lambdaClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                StatusCode: 500
            }))
        }
        const maybeResponse = await request(lambdaClient as any, functionName)(testPayload)

        expect(maybeResponse.isEmpty).toBeTruthy()
        expect(maybeResponse.getError()).toStrictEqual(new InvocationError('InvocationStatusCode', 500))
        expect(lambdaClient.send.mock.calls.length).toEqual(1)
        expect(lambdaClient.send.mock.calls[0][0] instanceof InvokeCommand)
        expect(lambdaClient.send.mock.calls[0][0].input).toEqual({
            FunctionName: functionName,
            InvocationType: InvocationType.RequestResponse,
            Payload: encoder.encode(JSON.stringify({request: testPayload}))
        })
    })
})

describe('InvocationError', () => {
    it('should construct an InvocationError', () => {
        const error = new InvocationError('NotFound', 'Something was not found')

        expect(error instanceof Error).toBeTruthy()
        expect(error.errorType).toEqual('NotFound')
        expect(error.name).toEqual('NotFound')
        expect(error.message).toEqual('Something was not found')
    })
})
