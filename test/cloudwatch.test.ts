import {
    countFetchStatus,
    countFunctionInvocation,
    incrementFunctionInvocationCount,
    MetricCondition
} from '../src/cloudwatch'
import {PutMetricDataCommand} from '@aws-sdk/client-cloudwatch'

describe('countFunctionInvocation', () => {
    const salutation = (message) => `The message is ${message}`

    it('should submit a count metric to cloudwatch', () => {
        const send = jest.fn()
        const cloudwatchClient = {send} as any

        const result = countFunctionInvocation(cloudwatchClient)({
            name: 'Invocation',
            namespace: 'Cocoa/Test'
        })(salutation)('salutation')('Hello World')

        expect(result).toEqual('The message is Hello World')
        expect(send).toBeCalledTimes(1)
        expect(send.mock.calls[0][0] instanceof PutMetricDataCommand).toBeTruthy()
        expect(send.mock.calls[0][0].input).toStrictEqual({
            Namespace: 'Cocoa/Test',
            MetricData: [
                {
                    MetricName: 'Invocation',
                    Unit: 'Count',
                    Value: 1,
                    Dimensions: [{Name: 'Function', Value: 'salutation'}]
                }
            ]
        })
    })
})

describe('incrementFunctionInvocationCount', () => {
    it('should submit a count metric to cloudwatch', async () => {
        const send = jest.fn().mockResolvedValue(true)
        const cloudwatchClient = {send} as any

        const result = await incrementFunctionInvocationCount(cloudwatchClient)({
            name: 'Invocation',
            namespace: 'Cocoa/Test'
        })('salutation')()

        expect(result.get()).toBeTruthy()
        expect(result.isPresent).toBeTruthy()
        expect(send).toBeCalledTimes(1)
        expect(send.mock.calls[0][0] instanceof PutMetricDataCommand).toBeTruthy()
        expect(send.mock.calls[0][0].input).toStrictEqual({
            Namespace: 'Cocoa/Test',
            MetricData: [
                {
                    MetricName: 'Invocation',
                    Unit: 'Count',
                    Value: 1,
                    Dimensions: [{Name: 'Function', Value: 'salutation'}]
                }
            ]
        })
    })

    it('should submit a custom count metric to cloudwatch', async () => {
        const send = jest.fn().mockResolvedValue(true)
        const cloudwatchClient = {send} as any

        const result = await incrementFunctionInvocationCount(cloudwatchClient)({
            name: 'Invocation',
            namespace: 'Cocoa/Test'
        })('salutation')(10)

        expect(result.get()).toBeTruthy()
        expect(result.isPresent).toBeTruthy()
        expect(send).toBeCalledTimes(1)
        expect(send.mock.calls[0][0] instanceof PutMetricDataCommand).toBeTruthy()
        expect(send.mock.calls[0][0].input).toStrictEqual({
            Namespace: 'Cocoa/Test',
            MetricData: [
                {
                    MetricName: 'Invocation',
                    Unit: 'Count',
                    Value: 10,
                    Dimensions: [{Name: 'Function', Value: 'salutation'}]
                }
            ]
        })
    })
})

describe('countFetchStatus', () => {
    it('should increase the counter if the response status is within range', async () => {
        const send = jest.fn()
        const cloudwatchClient = {send} as any

        const fetch = jest.fn().mockResolvedValue({ status: 200 })

        const input: Array<MetricCondition> = [
            {
                namespace: 'Cocoa/Test',
                name: '5XX',
                metricApplies: (response) => response.status >= 500
            },
            {
                namespace: 'Cocoa/Test',
                name: '2XX',
                metricApplies: (response) => response.status >= 200 && response.status < 300
            },
            {
                namespace: 'Cocoa/Test',
                name: '200',
                metricApplies: (response) => response.status === 200
            },
        ]

        const result = await countFetchStatus(cloudwatchClient)(fetch)(input)('fetch')('https://example.com')

        expect(result).toStrictEqual({ status: 200})
        expect(send).toBeCalledTimes(2)

        expect(send.mock.calls[0][0] instanceof PutMetricDataCommand).toBeTruthy()
        expect(send.mock.calls[1][0] instanceof PutMetricDataCommand).toBeTruthy()

        expect(send.mock.calls[0][0].input).toStrictEqual({
            Namespace: 'Cocoa/Test',
            MetricData: [
                {
                    MetricName: '2XX',
                    Unit: 'Count',
                    Value: 1,
                    Dimensions: [{Name: 'Function', Value: 'fetch'}]
                }
            ]
        })

        expect(send.mock.calls[1][0].input).toStrictEqual({
            Namespace: 'Cocoa/Test',
            MetricData: [
                {
                    MetricName: '200',
                    Unit: 'Count',
                    Value: 1,
                    Dimensions: [{Name: 'Function', Value: 'fetch'}]
                }
            ]
        })
    })
})
