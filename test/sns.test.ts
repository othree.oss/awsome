import {sns} from '../src'
import {SNSConfiguration} from '../src/sns'
import {PublishBatchCommand, PublishCommand, SubscribeCommand, UnsubscribeCommand} from '@aws-sdk/client-sns'
import {SetQueueAttributesCommand} from '@aws-sdk/client-sqs'

describe('send', () => {
    const configuration: SNSConfiguration = {
        TopicArn: 'aws:arn:sns:AwesomeTopic',
        BatchSize: 10,
        Parallelism: 10,
    }

    interface ExamplePayload {
        readonly name: string
        readonly lastName: string
    }

    it('should send the provided payload as a message', async () => {
        const snsClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                MessageId: 'm355463',
                SequenceNumber: '1',
            })),
        }

        const message: ExamplePayload = {
            name: 'Chuck',
            lastName: 'Norris',
        }

        const maybeResult = await sns.send<ExamplePayload>(snsClient as any, configuration)()()()(message)

        expect(maybeResult.isPresent).toBeTruthy()
        expect(maybeResult.get()).toBeTruthy()

        expect(snsClient.send.mock.calls.length).toEqual(1)
        expect(snsClient.send.mock.calls[0][0] instanceof PublishCommand).toBeTruthy()
        expect(snsClient.send.mock.calls[0][0].input).toStrictEqual({
            TopicArn: configuration.TopicArn,
            Message: JSON.stringify(message),
            MessageAttributes: undefined,
            MessageDeduplicationId: undefined,
            MessageGroupId: undefined,
        })
    })

    it('should send the provided payload as a message with the extra metadata', async () => {
        const snsClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                MessageId: 'm355463',
                SequenceNumber: '1',
            })),
        }

        const message: ExamplePayload = {
            name: 'Chuck',
            lastName: 'Norris',
        }

        const maybeResult = await sns.send<ExamplePayload>(snsClient as any, configuration)
        ((message: ExamplePayload) => message.lastName)
        ((message: ExamplePayload) => message.name)
        ((message: ExamplePayload) => {
            return {
                'celebrity': {
                    DataType: 'String',
                    StringValue: `${message.name} ${message.lastName}`,
                },
            }
        })(message)

        expect(maybeResult.isPresent).toBeTruthy()
        expect(maybeResult.get()).toBeTruthy()

        expect(snsClient.send.mock.calls.length).toEqual(1)
        expect(snsClient.send.mock.calls[0][0] instanceof PublishCommand).toBeTruthy()
        expect(snsClient.send.mock.calls[0][0].input).toStrictEqual({
            TopicArn: configuration.TopicArn,
            Message: JSON.stringify(message),
            MessageAttributes: {
                celebrity: {
                    DataType: 'String',
                    StringValue: 'Chuck Norris',
                },
            },
            MessageDeduplicationId: 'Chuck',
            MessageGroupId: 'Norris',
        })
    })
})

describe('sendAll', () => {
    const configuration: SNSConfiguration = {
        TopicArn: 'aws:arn:sns:AwesomeTopic',
        BatchSize: 2,
        Parallelism: 10,
    }

    interface ExamplePayload {
        readonly name: string
        readonly lastName: string
    }

    it('should send the provided payload as a message and return the successful and failed messages', async () => {
        const snsClient = {
            send: jest.fn().mockReturnValueOnce(Promise.resolve({
                Successful: [
                    {
                        Id: 'Chuck Norris',
                    },
                ],
                Failed: [
                    {
                        Id: 'Bruce Lee',
                    },
                ],
            }))
                .mockReturnValueOnce(Promise.reject({
                    $metadata: {
                        httpStatusCode: 500,
                    },
                })),
        }

        const messages: Array<ExamplePayload> = [
            {
                name: 'Chuck',
                lastName: 'Norris',
            },
            {
                name: 'Bruce',
                lastName: 'Lee',
            },
            {
                name: 'Jackie',
                lastName: 'Chan',
            },
            {
                name: 'Jean-Claude',
                lastName: 'Van Damme',
            },
        ]

        const maybeResult = await sns.sendAll<ExamplePayload>(snsClient as any, configuration)
        (message => `${message.name} ${message.lastName}`)()()()(messages)

        expect(maybeResult.isPresent).toBeTruthy()
        expect(maybeResult.get()).toStrictEqual({
            successful: ['Chuck Norris'],
            failed: ['Bruce Lee', 'Jackie Chan', 'Jean-Claude Van Damme'],
        })

        expect(snsClient.send.mock.calls.length).toEqual(2)
        expect(snsClient.send.mock.calls[0][0] instanceof PublishBatchCommand).toBeTruthy()
        expect(snsClient.send.mock.calls[0][0].input).toStrictEqual({
            TopicArn: configuration.TopicArn,
            PublishBatchRequestEntries: [
                {
                    Id: 'Chuck Norris',
                    Message: JSON.stringify(messages[0]),
                    MessageAttributes: undefined,
                    MessageDeduplicationId: undefined,
                    MessageGroupId: undefined,
                },
                {
                    Id: 'Bruce Lee',
                    Message: JSON.stringify(messages[1]),
                    MessageAttributes: undefined,
                    MessageDeduplicationId: undefined,
                    MessageGroupId: undefined,
                },
            ],
        })

        expect(snsClient.send.mock.calls[1][0] instanceof PublishBatchCommand).toBeTruthy()
        expect(snsClient.send.mock.calls[1][0].input).toStrictEqual({
            TopicArn: configuration.TopicArn,
            PublishBatchRequestEntries: [
                {
                    Id: 'Jackie Chan',
                    Message: JSON.stringify(messages[2]),
                    MessageAttributes: undefined,
                    MessageDeduplicationId: undefined,
                    MessageGroupId: undefined,
                },
                {
                    Id: 'Jean-Claude Van Damme',
                    Message: JSON.stringify(messages[3]),
                    MessageAttributes: undefined,
                    MessageDeduplicationId: undefined,
                    MessageGroupId: undefined,
                },
            ],
        })
    })

    it('should send the provided payload as a message with extra metadata and return the successful and failed messages', async () => {
        const snsClient = {
            send: jest.fn().mockReturnValueOnce(Promise.resolve({
                Successful: [
                    {
                        Id: 'Chuck Norris',
                    },
                ],
                Failed: [
                    {
                        Id: 'Bruce Lee',
                    },
                ],
            }))
                .mockReturnValueOnce(Promise.reject({
                    $metadata: {
                        httpStatusCode: 500,
                    },
                })),
        }

        const messages: Array<ExamplePayload> = [
            {
                name: 'Chuck',
                lastName: 'Norris',
            },
            {
                name: 'Bruce',
                lastName: 'Lee',
            },
            {
                name: 'Jackie',
                lastName: 'Chan',
            },
            {
                name: 'Jean-Claude',
                lastName: 'Van Damme',
            },
        ]

        const maybeResult = await sns.sendAll<ExamplePayload>(snsClient as any, configuration)
        (message => `${message.name} ${message.lastName}`)
        ((message: ExamplePayload) => message.lastName)
        ((message: ExamplePayload) => message.name)
        ((message: ExamplePayload) => {
            return {
                'celebrity': {
                    DataType: 'String',
                    StringValue: `${message.name} ${message.lastName}`,
                },
            }
        })(messages)

        expect(maybeResult.isPresent).toBeTruthy()
        expect(maybeResult.get()).toStrictEqual({
            successful: ['Chuck Norris'],
            failed: ['Bruce Lee', 'Jackie Chan', 'Jean-Claude Van Damme'],
        })

        expect(snsClient.send.mock.calls.length).toEqual(2)
        expect(snsClient.send.mock.calls[0][0] instanceof PublishBatchCommand).toBeTruthy()
        expect(snsClient.send.mock.calls[0][0].input).toStrictEqual({
            TopicArn: configuration.TopicArn,
            PublishBatchRequestEntries: [
                {
                    Id: 'Chuck Norris',
                    Message: JSON.stringify(messages[0]),
                    MessageAttributes: {
                        celebrity: {
                            DataType: 'String',
                            StringValue: 'Chuck Norris',
                        },
                    },
                    MessageDeduplicationId: 'Chuck',
                    MessageGroupId: 'Norris',
                },
                {
                    Id: 'Bruce Lee',
                    Message: JSON.stringify(messages[1]),
                    MessageAttributes: {
                        celebrity: {
                            DataType: 'String',
                            StringValue: 'Bruce Lee',
                        },
                    },
                    MessageDeduplicationId: 'Bruce',
                    MessageGroupId: 'Lee',
                },
            ],
        })

        expect(snsClient.send.mock.calls[1][0] instanceof PublishBatchCommand).toBeTruthy()
        expect(snsClient.send.mock.calls[1][0].input).toStrictEqual({
            TopicArn: configuration.TopicArn,
            PublishBatchRequestEntries: [
                {
                    Id: 'Jackie Chan',
                    Message: JSON.stringify(messages[2]),
                    MessageAttributes: {
                        celebrity: {
                            DataType: 'String',
                            StringValue: 'Jackie Chan',
                        },
                    },
                    MessageDeduplicationId: 'Jackie',
                    MessageGroupId: 'Chan',
                },
                {
                    Id: 'Jean-Claude Van Damme',
                    Message: JSON.stringify(messages[3]),
                    MessageAttributes: {
                        celebrity: {
                            DataType: 'String',
                            StringValue: 'Jean-Claude Van Damme',
                        },
                    },
                    MessageDeduplicationId: 'Jean-Claude',
                    MessageGroupId: 'Van Damme',
                },
            ],
        })
    })
})

describe('subscribeSqs', () => {
    const configuration: SNSConfiguration = {
        TopicArn: 'aws:arn:sns:AwesomeTopic',
        BatchSize: 10,
        Parallelism: 10,
    }

    it('should subscribe a queue to the topic', async () => {
        const snsClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({
                SubscriptionArn: 'aws:arn:sns:AwesomeTopic:5ub5cr1710n'
            })),
        }

        const sqsClient = {
            send: jest.fn().mockReturnValue(Promise.resolve(true))
        }

        const maybeResult = await sns.subscribeSqs(snsClient as any, sqsClient as any, configuration)({
            queueArn: 'arn:aws:sqs:us-west-2:12345:TestQueue',
            queueUrl: 'https://sqs.us-west-2.amazonaws.com/12345/TestQueue'
        })

        expect(maybeResult.isPresent).toBeTruthy()
        expect(maybeResult.get()).toStrictEqual({
            subscriptionArn: 'aws:arn:sns:AwesomeTopic:5ub5cr1710n'
        })

        expect(snsClient.send).toBeCalledTimes(1)
        expect(snsClient.send.mock.calls[0][0] instanceof SubscribeCommand).toBeTruthy()
        expect(snsClient.send.mock.calls[0][0].input).toStrictEqual({
            TopicArn: configuration.TopicArn,
            Protocol: 'sqs',
            Endpoint: 'arn:aws:sqs:us-west-2:12345:TestQueue',
            Attributes: {
                RawMessageDelivery: 'false',
            },
        })

        expect(sqsClient.send).toBeCalledTimes(1)
        expect(sqsClient.send.mock.calls[0][0] instanceof SetQueueAttributesCommand).toBeTruthy()
        expect(sqsClient.send.mock.calls[0][0].input).toStrictEqual({
            QueueUrl: 'https://sqs.us-west-2.amazonaws.com/12345/TestQueue',
            Attributes: {
                Policy: JSON.stringify({
                    Version: '2012-10-17',
                    Statement: [
                        {
                            Effect: 'Allow',
                            Principal: {
                                Service: 'sns.amazonaws.com'
                            },
                            Action: "sqs:SendMessage",
                            Resource: 'arn:aws:sqs:us-west-2:12345:TestQueue',
                            Condition: {
                                ArnEquals: {
                                    'aws:SourceArn': configuration.TopicArn
                                }
                            }
                        }
                    ]
                })
            }
        })
    })
})

describe('unsubscribe', () => {
    it('should unsubscribe from the topic', async () => {
        const snsClient = {
            send: jest.fn().mockReturnValue(Promise.resolve({})),
        }

        const maybeResult = await sns.unsubscribe(snsClient as any)({
            subscriptionArn: 'arn:aws:sns:us-west-2:12345:sub1'
        })

        expect(maybeResult.isPresent).toBeTruthy()
        expect(maybeResult.get()).toBeTruthy()

        expect(snsClient.send).toBeCalledTimes(1)
        expect(snsClient.send.mock.calls[0][0] instanceof UnsubscribeCommand).toBeTruthy()
        expect(snsClient.send.mock.calls[0][0].input).toStrictEqual({
            SubscriptionArn: 'arn:aws:sns:us-west-2:12345:sub1'
        })
    })
})
