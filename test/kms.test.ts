import {kms} from '../src'
import {GetPublicKeyCommand, SignCommand, SigningAlgorithmSpec} from '@aws-sdk/client-kms'

describe('sign', () => {
    const configuration: kms.SignConfiguration = {
        KeyId: 'aws:kms:Secret',
        SigningAlgorithm: SigningAlgorithmSpec.RSASSA_PKCS1_V1_5_SHA_256
    }

    it('should sign the specified message', async () => {
        const expectedSignature = new Uint8Array([1,2,3])
        const kmsClient = {
            send: jest.fn().mockResolvedValue({
                Signature: expectedSignature
            })
        }
        const maybeSignature = await kms.sign(kmsClient as any)(configuration)('The message')

        expect(maybeSignature.isPresent).toBeTruthy()
        expect(maybeSignature.get()).toStrictEqual(expectedSignature)

        expect(kmsClient.send).toBeCalledTimes(1)
        expect(kmsClient.send.mock.calls[0][0] instanceof SignCommand).toBeTruthy()
        expect(kmsClient.send.mock.calls[0][0].input).toStrictEqual({
            Message: Buffer.from('The message'),
            KeyId: configuration.KeyId,
            SigningAlgorithm: configuration.SigningAlgorithm,
            MessageType: 'RAW'
        })
    })
})

describe('getPublicKey', () => {
    const configuration: kms.SignConfiguration = {
        KeyId: 'aws:kms:Secret',
        SigningAlgorithm: SigningAlgorithmSpec.RSASSA_PKCS1_V1_5_SHA_256
    }

    it('should get the public key', async () => {
        const expectedKey = new Uint8Array([1,2,3])
        const kmsClient = {
            send: jest.fn().mockResolvedValue({
                PublicKey: expectedKey
            })
        }
        const maybeSignature = await kms.getPublicKey(kmsClient as any)('aws:kms:Secret')()

        expect(maybeSignature.isPresent).toBeTruthy()
        expect(maybeSignature.get()).toStrictEqual(expectedKey)

        expect(kmsClient.send).toBeCalledTimes(1)
        expect(kmsClient.send.mock.calls[0][0] instanceof GetPublicKeyCommand).toBeTruthy()
        expect(kmsClient.send.mock.calls[0][0].input).toStrictEqual({
            KeyId: 'aws:kms:Secret'
        })
    })
})
