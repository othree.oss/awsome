import {Optional, TryAsync} from '@othree.io/optional'
import {GetPublicKeyCommand, KMSClient, SignCommand, SigningAlgorithmSpec} from '@aws-sdk/client-kms'

export interface SignConfiguration {
    readonly KeyId: string
    readonly SigningAlgorithm: SigningAlgorithmSpec
}

export const sign = (kmsClient: KMSClient) =>
    (configuration: SignConfiguration) =>
        async (message: string): Promise<Optional<Uint8Array>> => {
            return TryAsync(async () => {
                const result = await kmsClient.send(new SignCommand({
                    Message: Buffer.from(message),
                    KeyId: configuration.KeyId,
                    SigningAlgorithm: configuration.SigningAlgorithm,
                    MessageType: 'RAW'
                }))

                return result.Signature!
            })
        }

export const getPublicKey = (kmsClient: KMSClient) =>
    (keyId: string) =>
        async (): Promise<Optional<Uint8Array>> => {
            return TryAsync(async () => {
                const result = await kmsClient.send(new GetPublicKeyCommand({
                    KeyId: keyId
                }))

                return result.PublicKey!
            })
        }
