import {APIGatewayClient, ApiKey, GetApiKeysCommand} from '@aws-sdk/client-api-gateway'
import {Optional, TryAsync} from '@othree.io/optional'

const API_KEYS_LIMIT = 500

export interface Tag {
    readonly [key: string]: string
}

export interface KeyTags {
    readonly keyId: string
    readonly tags: Tag
}

export const getAllApiKeysTags = (client: APIGatewayClient) =>
    async (): Promise<Optional<Array<KeyTags>>> => {
        const getKeys = async (position?: string): Promise<Array<ApiKey>> => {
            const keys = await client.send(new GetApiKeysCommand({
                limit: API_KEYS_LIMIT,
                position: position
            }))

            if (Optional(keys.position).isPresent) {
                const newKeys = await getKeys(keys.position)
                return [...Optional(keys.items).orElse([]), ...newKeys]
            }

            return Optional(keys.items).orElse([])
        }

        return TryAsync(async () => {
            return (await getKeys()).map(key => {
                return {
                    keyId: key.id!,
                    tags: Optional(key.tags as Tag).orElse({})
                }
            })
        })
    }
