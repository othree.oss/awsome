import {InvocationType, InvokeCommand, LambdaClient} from '@aws-sdk/client-lambda'
import {Optional, TryAsync} from '@othree.io/optional'

const encoder = new TextEncoder()
const decoder = new TextDecoder()

export class InvocationError extends Error {
    errorType: string

    constructor(errorType, ...params) {
        super(...params)

        /* istanbul ignore next */
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, InvocationError)
        }

        this.name = errorType
        this.errorType = errorType
    }
}

export const invoke = <Input, Output>(lambda: LambdaClient, functionName: string) => {
    Optional(functionName).orThrow(new Error('functionName is required'))
    return async (payload: Input): Promise<Optional<Output>> => {
        return TryAsync(async () => {
            const command = new InvokeCommand({
                FunctionName: functionName,
                InvocationType: InvocationType.RequestResponse,
                Payload: encoder.encode(JSON.stringify(payload))
            })

            const result = await lambda.send(command)
            if (result.StatusCode === 200) {
                const resultPayload = JSON.parse(decoder.decode(result.Payload))
                if (!result.FunctionError) {
                    return resultPayload as Output
                }

                throw new InvocationError(resultPayload.errorType, resultPayload.errorMessage)
            }

            throw new InvocationError('InvocationStatusCode', result.StatusCode)
        })
    }
}

export interface Query<T> {
    readonly query: T
}

export const query = <Input, Output>(lambda: LambdaClient, functionName: string) => {
    const invoker = invoke<Query<Input>, Output>(lambda, functionName)
    return async (payload: Input): Promise<Optional<Output>> => {
        return invoker({
            query: payload
        })
    }
}

export interface Command<T> {
    readonly command: T
}

export const command = <Input, Output>(lambda: LambdaClient, functionName: string) => {
    const invoker = invoke<Command<Input>, Output>(lambda, functionName)
    return async (payload: Input): Promise<Optional<Output>> => {
        return invoker({
            command: payload
        })
    }
}

export interface Request<T> {
    readonly request: T
}

export const request = <Input, Output>(lambda: LambdaClient, functionName: string) => {
    const invoker = invoke<Request<Input>, Output>(lambda, functionName)
    return async (payload: Input): Promise<Optional<Output>> => {
        return invoker({
            request: payload
        })
    }
}
