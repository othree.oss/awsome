/** @module sns */
import {
    SNSClient,
    PublishCommand,
    MessageAttributeValue,
    PublishBatchCommand,
    SubscribeCommand, UnsubscribeCommand,
} from '@aws-sdk/client-sns'
import {TryAsync, Optional} from '@othree.io/optional'
import {fragment} from './utils'
import {mapLimit} from 'async'
import {SetQueueAttributesCommand, SQSClient} from '@aws-sdk/client-sqs'

/**
 * Configuration options for sending messages to an SNS topic.
 * @interface
 * @property {string} TopicArn - The ARN of the SNS topic to send messages to.
 * @property {number} BatchSize - The batch size for sending messages.
 * @property {number} Parallelism - The level of parallelism for sending messages.
 */
export interface SNSConfiguration {
    readonly TopicArn: string
    readonly BatchSize: number
    readonly Parallelism: number
}

/**
 * Creates a function that sends a message to an SNS topic.
 * @template T
 * @param {SNSClient} client - The SNS client used to send the message.
 * @param {SNSConfiguration} configuration - The configuration for sending messages.
 * @returns {Function} A series of functions that configure and send the message.
 * @async
 * @example
 * const snsClient = new SNSClient({ region: 'us-east-1' })
 * const sendConfig = {
 *     TopicArn: 'arn:aws:sns:us-east-1:123456789012:my-topic',
 *     BatchSize: 10,
 *     Parallelism: 5
 * }
 * const sendMessageFunction = send(snsClient, sendConfig)
 * const withMessageGroupId = (message) => 'group-id'
 * const withMessageDeduplicationId = (message) => 'dedup-id'
 * const withAttributes = (message) => ({ AttributeName: 'AttributeValue' })
 * const sendPayload = { key: 'value' }
 * const result = await sendMessageFunction(withMessageGroupId)(withMessageDeduplicationId)(withAttributes)(sendPayload)
 * console.log('Message sent successfully:', result.get())
 */
export const send = <T>(client: SNSClient, configuration: SNSConfiguration) =>
    /**
     * Configures the deduplication ID for the message.
     * @param {function} withMessageDeduplicationId - The function to extract the deduplication ID.
     * @returns {Function} A function that accepts optional configuration functions and sends the prepared payload.
     */
    (withMessageGroupId?: (T) => string) =>
        /**
         * Configures the deduplication ID for the message.
         * @param {function} withMessageDeduplicationId - The function to extract the deduplication ID.
         * @returns {Function} A function that accepts optional configuration functions and sends the prepared payload.
         */
        (withMessageDeduplicationId?: (T) => string) =>
            /**
             * Configures custom attributes for the message.
             * @param {function} withAttributes - The function to extract message attributes.
             * @returns {Function} A function that accepts the payload and sends the message.
             */
            (withAttributes?: (T) => { [key: string]: MessageAttributeValue }) =>
                /**
                 * Sends a message to the configured SNS topic.
                 * @param {T} payload - The payload of the message to be sent.
                 * @returns {Promise<Optional<boolean>>} A promise that resolves to an optional boolean indicating the success of the send operation.
                 * @async
                 */
                (payload: T): Promise<Optional<boolean>> => {
                    return TryAsync(async () => {
                        const sendMessageCommand = new PublishCommand({
                            TopicArn: configuration.TopicArn,
                            Message: JSON.stringify(payload),
                            MessageGroupId: Optional(withMessageGroupId).map(_ => _!(payload)).orElse(undefined!),
                            MessageDeduplicationId: Optional(withMessageDeduplicationId).map(_ => _!(payload)).orElse(undefined!),
                            MessageAttributes: Optional(withAttributes).map(_ => _!(payload)).orElse(undefined!),
                        })

                        const publishResponse = await client.send(sendMessageCommand)
                        console.debug('Published message. Id: ', publishResponse.MessageId)
                        return true
                    })
                }

/**
 * Represents the result of a batch message sending operation.
 * @interface
 * @property {Array<string>} successful - An array of message IDs that were successfully sent.
 * @property {Array<string>} failed - An array of message IDs that failed to send.
 */
export interface MessageBatchResult {
    readonly successful: Array<string>
    readonly failed: Array<string>
}

/**
 * Creates a function that sends an array of messages to an SNS topic in batches.
 * @template T
 * @param {SNSClient} client - The SNS client used to send the messages.
 * @param {SNSConfiguration} configuration - The configuration for sending messages.
 * @returns {Function} A series of functions that configure and send the messages in batches.
 * @async
 * @example
 * const snsClient = new SNSClient({ region: 'us-east-1' })
 * const sendAllConfig = {
 *     TopicArn: 'arn:aws:sns:us-east-1:123456789012:my-topic',
 *     BatchSize: 10,
 *     Parallelism: 5
 * }
 * const sendAllFunction = sendAll(snsClient, sendAllConfig)
 * const withId = (message) => 'message-id'
 * const withMessageGroupId = (message) => 'group-id'
 * const withMessageDeduplicationId = (message) => 'dedup-id'
 * const withAttributes = (message) => ({ AttributeName: 'AttributeValue' })
 * const messages = [{ key: 'value' }, { key: 'value' }]
 * const result = await sendAllFunction(withId)(withMessageGroupId)(withMessageDeduplicationId)(withAttributes)(messages)
 * console.log('Batch message send result:', result.get())
 */
export const sendAll = <T>(client: SNSClient, configuration: SNSConfiguration) =>
    /**
     * Configures and sends an array of messages to an SNS topic in batches.
     * @param {function} withId - The function to extract the message ID.
     * @returns {Function} A function that accepts optional configuration functions and sends the messages in batches.
     * @async
     */
    (withId: (T) => string) =>
        /**
         * Configures the message group ID for the messages.
         * @param {function} withMessageGroupId - The function to extract the message group ID.
         * @returns {Function} A function that accepts optional configuration functions and sends the messages in batches.
         */
        (withMessageGroupId?: (T) => string) =>
            /**
             * Configures the deduplication ID for the messages.
             * @param {function} withMessageDeduplicationId - The function to extract the deduplication ID.
             * @returns {Function} A function that accepts optional configuration functions and sends the messages in batches.
             */
            (withMessageDeduplicationId?: (T) => string) =>
                /**
                 * Configures custom attributes for the messages.
                 * @param {function} withAttributes - The function to extract message attributes.
                 * @returns {Function} A function that accepts the array of messages and sends them in batches.
                 * @async
                 */
                (withAttributes?: (T) => { [key: string]: MessageAttributeValue }) =>
                    /**
                     * Sends an array of messages to the configured SNS topic in batches.
                     * @param {Array<T>} payload - The array of messages to be sent.
                     * @returns {Promise<Optional<MessageBatchResult>>} A promise that resolves to an optional MessageBatchResult object.
                     * @async
                     */
                    async (payload: Array<T>): Promise<Optional<MessageBatchResult>> => {
                        const batches = fragment(payload, configuration.BatchSize)
                        const results: Array<MessageBatchResult> = await mapLimit(batches, configuration.Parallelism, async (messages): Promise<MessageBatchResult> => {
                            return (await TryAsync(async () => {
                                try {
                                    const command = new PublishBatchCommand({
                                        TopicArn: configuration.TopicArn,
                                        PublishBatchRequestEntries: messages.map(message => {
                                            return {
                                                Id: withId(message),
                                                Message: JSON.stringify(message),
                                                MessageGroupId: Optional(withMessageGroupId).map(_ => _!(message)).orElse(undefined!),
                                                MessageDeduplicationId: Optional(withMessageDeduplicationId).map(_ => _!(message)).orElse(undefined!),
                                                MessageAttributes: Optional(withAttributes).map(_ => _!(message)).orElse(undefined!),
                                            }
                                        }),
                                    })

                                    const publishResponse = await client.send(command)
                                    /* istanbul ignore next */
                                    console.debug('Published messages. Successful messages: ', publishResponse.Successful?.length, 'Failed messages: ', publishResponse.Failed?.length)
                                    return {
                                        successful: Optional(publishResponse.Successful).map(_ => _!.map(m => m.Id!)).orElse([] as Array<string>),
                                        failed: Optional(publishResponse.Failed).map(_ => _!.map(m => m.Id!)).orElse([] as Array<string>),
                                    }
                                } catch (error) {
                                    console.error('Cannot send message', error)
                                    return {
                                        successful: [] as Array<string>,
                                        failed: messages.map(_ => withId(_)),
                                    }
                                }
                            })).get()
                        })

                        return TryAsync(async () => {
                            return results.reduce((acum, current) => {
                                return {
                                    successful: [...acum.successful, ...current.successful],
                                    failed: [...acum.failed, ...current.failed],
                                }
                            }, {
                                successful: [],
                                failed: [],
                            })
                        })
                    }

/**
 * Represents the request to subscribe an SQS queue to an SNS topic.
 * @interface
 * @property {string} queueArn - The ARN of the SQS queue to be subscribed.
 * @property {string} queueUrl - The URL of the SQS queue to be subscribed.
 */
export interface SubscribeQueueRequest {
    readonly queueArn: string
    readonly queueUrl: string
}

/**
 * Represents a subscription to an SNS topic.
 * @interface
 * @property {string} subscriptionArn - The ARN of the subscription.
 */
export interface Subscription {
    readonly subscriptionArn: string
}

/**
 * Creates a function that subscribes an SQS queue to an SNS topic.
 * @param {SNSClient} snsClient - The SNS client used to subscribe to the topic.
 * @param {SQSClient} sqsClient - The SQS client used to set queue attributes.
 * @param {SNSConfiguration} configuration - The configuration for subscribing to the topic.
 * @returns {Function} An asynchronous function that takes a SubscribeQueueRequest and returns a promise.
 * @async
 * @example
 * const snsClient = new SNSClient({ region: 'us-east-1' })
 * const sqsClient = new SQSClient({ region: 'us-east-1' })
 * const subscribeConfig = {
 *     TopicArn: 'arn:aws:sns:us-east-1:123456789012:my-topic',
 *     BatchSize: 10,
 *     Parallelism: 5
 * }
 * const subscribeFunction = subscribeSqs(snsClient, sqsClient, subscribeConfig)
 * const subscribeRequest = {
 *     queueArn: 'arn:aws:sqs:us-east-1:123456789012:my-queue',
 *     queueUrl: 'https://sqs.us-east-1.amazonaws.com/123456789012/my-queue'
 * }
 * const subscription = await subscribeFunction(subscribeRequest)
 * console.log('Subscription successful:', subscription.get())
 */
export const subscribeSqs = (snsClient: SNSClient, sqsClient: SQSClient, configuration: SNSConfiguration) =>
    /**
     * Subscribes an SQS queue to an SNS topic.
     * @param {SubscribeQueueRequest} request - The request to subscribe the SQS queue.
     * @returns {Promise<Optional<Subscription>>} A promise that resolves to an optional Subscription object.
     * @async
     */
    async (request: SubscribeQueueRequest): Promise<Optional<Subscription>> => {
        return TryAsync(async () => {
            const subscriptionResponse = await snsClient.send(new SubscribeCommand({
                TopicArn: configuration.TopicArn,
                Protocol: 'sqs',
                Endpoint: request.queueArn,
                Attributes: {
                    RawMessageDelivery: 'false',
                },
            }))

            await sqsClient.send(new SetQueueAttributesCommand({
                QueueUrl: request.queueUrl,
                Attributes: {
                    Policy: JSON.stringify({
                        Version: '2012-10-17',
                        Statement: [
                            {
                                Effect: 'Allow',
                                Principal: {
                                    Service: 'sns.amazonaws.com',
                                },
                                Action: 'sqs:SendMessage',
                                Resource: request.queueArn,
                                Condition: {
                                    ArnEquals: {
                                        'aws:SourceArn': configuration.TopicArn,
                                    },
                                },
                            },
                        ],
                    }),
                },
            }))

            return {
                subscriptionArn: subscriptionResponse.SubscriptionArn!,
            }
        })
    }

/**
 * Creates a function that removes a subscription
 * @param {SNSClient} snsClient - The SNS client used to unsubscribe from the topic.
 * @returns {Function} An asynchronous function that takes a Subscription and returns a promise.
 * @async
 * @example
 * const snsClient = new SNSClient({ region: 'us-east-1' })
 * const unsubscribeFunction = unsubscribe(snsClient)
 * const subscribeRequest = {
 *     subscriptionArn: 'arn:aws:sns:us-east-1:123456789012:subscription:1234'
 * }
 * const isSuccessful = await unsubscribeFunction(subscribeRequest)
 * console.log('Subscription successful:', subscription.get())
 */
export const unsubscribe = (snsClient: SNSClient) =>
    /**
     * Unsubscribes from an sns topic
     * @param {Subscription} subscription - The subscription to remove.
     * @returns {Promise<Optional<true>>} A promise that resolves to an optional true.
     * @async
     */
    async (subscription: Subscription): Promise<Optional<true>> => {
        return TryAsync<true>(async () => {
            const response = await snsClient.send(new UnsubscribeCommand({
                SubscriptionArn: subscription.subscriptionArn,
            }))

            return true
        })
    }
