import {TryAsync, Optional} from '@othree.io/optional'
import {SendRawEmailCommand, SESClient} from '@aws-sdk/client-ses'

/**
 * Returns an asynchronous function that sends a mime formatted email.
 * @param {SESClient} sesClient - The SES client used to send the email.
 * @returns {Function} A series of functions that configure and send the message.
 * @async
 **/
export const sendMimeEmail = (sesClient: SESClient) =>
    /**
     * Sends the formatted mime email using ses.
     *
     * @param {Uint8Array} mimeMessage - The mime email.
     * @returns {Promise<Optional<string>>} A Promise that resolves into the sent message id
     */
    (mimeMessage: Uint8Array): Promise<Optional<string>> => {
        return TryAsync(async () => {
            const request = new SendRawEmailCommand({
                RawMessage: {
                    Data: Buffer.from(mimeMessage)
                }
            })

            return sesClient.send(request)
                .then(_ => _.MessageId!)
        })
    }
