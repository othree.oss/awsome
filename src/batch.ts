import {BatchClient, DescribeJobsCommand, SubmitJobCommand} from '@aws-sdk/client-batch'
import {Optional, TryAsync} from '@othree.io/optional'

/**
 * Enumeration representing possible job statuses.
 * @enum {string}
 * @readonly
 */
export enum JobStatus {
    SUBMITTED = 'SUBMITTED',
    PENDING = 'PENDING',
    RUNNABLE = 'RUNNABLE',
    STARTING = 'STARTING',
    RUNNING = 'RUNNING',
    SUCCEEDED = 'SUCCEEDED',
    FAILED = 'FAILED'
}

/**
 * Represents a request to submit a job.
 * @interface
 */
export interface SubmitJobRequest {
    /**
     * The Amazon Resource Name (ARN) of the job.
     * @type {string}
     * @readonly
     */
    readonly jobArn: string

    /**
     * The Amazon Resource Name (ARN) of the job definition.
     * @type {string}
     * @readonly
     */
    readonly jobDefinitionArn: string

    /**
     * The Amazon Resource Name (ARN) of the job queue.
     * @type {string}
     * @readonly
     */
    readonly jobQueueArn: string
}

/**
 * Represents a response after submitting a job.
 * @interface
 */
export interface SubmitJobResponse {
    /**
     * The unique identifier for the submitted job.
     * @type {string}
     * @readonly
     */
    readonly jobId: string

    /**
     * The name of the submitted job.
     * @type {string}
     * @readonly
     */
    readonly jobName: string

    /**
     * The Amazon Resource Name (ARN) of the submitted job.
     * @type {string}
     * @readonly
     */
    readonly jobArn: string
}

/**
 * Returns an asynchronous function that submits a job to Amazon Batch.
 *
 * @param {BatchClient} client - The batch client used to interact with Amazon Batch.
 * @returns {Function} An asynchronous function that accepts `SubmitJobRequest` in order to submit the job.
 */
export const submitJob = (client: BatchClient) =>
    /**
     * Submits a job with the provided `SubmitJobRequest` to Amazon Batch.
     *
     * @param {SubmitJobRequest} submitJobRequest - The request to submit a job.
     * @returns {Promise<Optional<SubmitJobResponse>>} A Promise resolving to the submitted job properties
     */
    async (submitJobRequest: SubmitJobRequest): Promise<Optional<SubmitJobResponse>> => {
        return TryAsync(async () => {
            const submittedJob = await client.send(new SubmitJobCommand({
                jobName: submitJobRequest.jobArn,
                jobDefinition: submitJobRequest.jobDefinitionArn,
                jobQueue: submitJobRequest.jobQueueArn
            }))

            if (submittedJob.$metadata.httpStatusCode !== 200) {
                throw new Error(`Unexpected while submitting job, code: ${submittedJob.$metadata.httpStatusCode}`)
            }

            return {
                jobId: submittedJob.jobId!,
                jobName: submittedJob.jobName!,
                jobArn: submittedJob.jobArn!
            }
        })
    }

/**
 * Represents a request to retrieve the status of a job.
 * @interface
 */
export interface JobStatusRequest {
    /**
     * The unique identifier of the job for which the status is requested.
     * @type {string}
     * @readonly
     */
    readonly jobId: string
}

/**
 * Returns an asynchronous function that gets a job status from Amazon Batch.
 *
 * @param {BatchClient} client - The batch client used to interact with Amazon Batch.
 * @returns {Function} An asynchronous function that accepts `JobStatusRequest` in order to get the job status.
 */
export const getJobStatus = (client: BatchClient) =>
    /**
     * Submits a job with the provided `SubmitJobRequest` to Amazon Batch.
     *
     * @param {JobStatusRequest} jobStatusRequest - The request to get a job status.
     * @returns {Promise<Optional<JobStatus>>} A Promise resolving to the job status
     */
    async (jobStatusRequest: JobStatusRequest): Promise<Optional<JobStatus>> => {
        return TryAsync(async () => {
            const jobStatuses = await client.send(new DescribeJobsCommand({
                jobs: [jobStatusRequest.jobId]
            }))

            if (jobStatuses.$metadata.httpStatusCode !== 200 || jobStatuses.jobs?.length === 0) {
                throw new Error(`Unexpected while getting job status, code: ${jobStatuses.$metadata.httpStatusCode}`)
            }

            return JobStatus[jobStatuses.jobs![0].status!]
        })
    }
