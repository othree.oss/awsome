import {
    DynamoDBClient,
    PutItemCommand,
    QueryCommand,
    DeleteItemCommand,
    ScanCommand,
    BatchGetItemCommand
} from '@aws-sdk/client-dynamodb'
import {Optional, TryAsync} from '@othree.io/optional'
import {marshall, unmarshall} from '@aws-sdk/util-dynamodb'
import {KeysAndAttributes, AttributeValue} from '@aws-sdk/client-dynamodb/dist-types/models/models_0'

export interface DynamoConfiguration {
    readonly TableName: string
    readonly IndexName?: string
}

export type Keys = { [key: string]: any }

export const upsert = <Entity>(dynamoDb: DynamoDBClient, configuration: DynamoConfiguration) => async (entity: Entity): Promise<Optional<Entity>> => {
    return TryAsync(async () => {
        const command = new PutItemCommand({
            TableName: configuration.TableName,
            Item: marshall<Entity>(entity, {removeUndefinedValues: true})
        })

        await dynamoDb.send(command)

        return entity
    })
}

export const query = <Entity>(dynamoDb: DynamoDBClient, configuration: DynamoConfiguration) => async (keys: Keys, filters?: Keys): Promise<Optional<Array<Entity>>> => {
    return TryAsync(async () => {
        if(Object.keys(keys).length > 2) {
            throw new Error('Only partition key and sort key are allowed to be query on, if you need more, try filtering')
        }

        async function query(exclusiveStartKey?: { [key: string]: AttributeValue }) {
            const expressionAttributeNames = {
                ...Object.keys(keys).reduce((acum, current) => {
                    return {
                        ...acum,
                        [`#${current}`]: current
                    }
                }, {}),
                ...Optional(filters).map(_ => (Object.keys(_).reduce((acum, current) => {
                    return {
                        ...acum,
                        [`#${current}`]: current
                    }
                }, {}))).orElse({})
            }

            const expressionAttributeValues = {
                ...marshall(
                    Object.keys(keys).reduce((acum, current) => {
                        return {
                            ...acum,
                            [`:${current}`]: keys[current]
                        }
                    }, {})
                ),
                ...Optional(filters).map(_ => marshall(
                    Object.keys(_).reduce((acum, current) => {
                        return {
                            ...acum,
                            [`:${current}`]: filters![current]
                        }
                    }, {})
                )).orElse({})
            }

            const command = new QueryCommand({
                TableName: configuration.TableName,
                IndexName: configuration.IndexName,
                KeyConditionExpression: Object.keys(keys).map(key => `#${key} = :${key}`).join(' and '),
                FilterExpression: Optional(filters).map(_ => Object.keys(_).map(key => `#${key} = :${key}`).join(' and ')).orElse(undefined!),
                ExpressionAttributeNames: expressionAttributeNames,
                ExpressionAttributeValues: expressionAttributeValues,
                ExclusiveStartKey: exclusiveStartKey
            })

            const result = await dynamoDb.send(command)

            if (Optional(result.LastEvaluatedKey).isPresent) {
                const items = (result.Items!.map(_ => unmarshall(_))) as Array<Entity>
                const newItems = await query(result.LastEvaluatedKey)
                return [...items, ...newItems]
            }


            if (result.Items) {
                return (result.Items.map(_ => unmarshall(_))) as Array<Entity>
            }

            return []
        }

        return query()
    })
}

export const getBy = <Entity>(query: (keys: Keys, filters?: Keys) => Promise<Optional<Array<Entity>>>) =>
    async (keys: Keys): Promise<Optional<Array<Entity>>> => {
    return query(keys)
}

export const filterBy = <Entity>(query: (keys: Keys, filters?: Keys) => Promise<Optional<Array<Entity>>>) =>
    async (keys: Keys, filters: Keys): Promise<Optional<Array<Entity>>> => {
        return query(keys, filters)
    }

export const getBatchBy = <Entity>(dynamoDb: DynamoDBClient, configuration: DynamoConfiguration) => async (keys: Array<Keys>): Promise<Optional<Array<Entity>>> => {
    if (keys.length === 0) {
        return Optional([])
    }

    return TryAsync(async () => {
        async function query(unprocessedKeys?: Record<string, KeysAndAttributes>) {
            const command = new BatchGetItemCommand({
                RequestItems: Optional(unprocessedKeys).orElse({
                    [configuration.TableName]: {
                        Keys: keys.map(_ => marshall(_))
                    }
                })
            })

            const result = await dynamoDb.send(command)

            if (Object.keys(Optional(result.UnprocessedKeys).orElse({})).length > 0) {
                const items = (result.Responses![configuration.TableName].map(_ => unmarshall(_))) as Array<Entity>
                const newItems = await query(result.UnprocessedKeys)
                return [...items, ...newItems]
            }


            if (result.Responses) {
                return (result.Responses![configuration.TableName].map(_ => unmarshall(_))) as Array<Entity>
            }

            return []
        }

        return query()
    })
}

export const deleteBy = <Entity>(dynamoDb: DynamoDBClient, configuration: DynamoConfiguration) => async (keys: Keys): Promise<Optional<boolean>> => {
    return TryAsync(async () => {
        const command = new DeleteItemCommand({
            TableName: configuration.TableName,
            Key: marshall(keys)
        })

        await dynamoDb.send(command)
        return true
    })
}

export const getAll = <Entity>(dynamoDb: DynamoDBClient, configuration: DynamoConfiguration) => async (): Promise<Optional<Array<Entity>>> => {
    return TryAsync(async () => {
        async function query(exclusiveStartKey?: Record<string, AttributeValue>) {

            const command = new ScanCommand({
                TableName: configuration.TableName,
                IndexName: configuration.IndexName,
                ExclusiveStartKey: exclusiveStartKey
            })

            const result = await dynamoDb.send(command)

            if (Optional(result.LastEvaluatedKey).isPresent) {
                const items = (result.Items!.map(_ => unmarshall(_))) as Array<Entity>
                const newItems = await query(result.LastEvaluatedKey)
                return [...items, ...newItems]
            }


            if (result.Items) {
                return (result.Items.map(_ => unmarshall(_))) as Array<Entity>
            }

            return []
        }

        return query()
    })
}
