
export function fragment(array: Array<any>, fragmentSize: number, startIndex: number = 0 , initialArray: Array<any> = []) : Array<Array<any>>{
    const result: Array<Array<any>> = initialArray
    let initialIndex = startIndex
    while(1 === 1) {
        const arrayFragment = array.slice(initialIndex, initialIndex + fragmentSize)
        if (arrayFragment.length === 0){
            break
        }
        result.push(arrayFragment)
        initialIndex = initialIndex + fragmentSize
    }

    return result
}
