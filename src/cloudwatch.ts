import {CloudWatchClient, PutMetricDataCommand} from '@aws-sdk/client-cloudwatch'
import {Optional, TryAsync} from '@othree.io/optional'

export interface MetricDefinition {
    readonly namespace: string
    readonly name: string
}

export interface MetricCondition extends MetricDefinition {
    readonly metricApplies: (response: Response) => boolean
}

export type Fetch = (input: RequestInfo, init?: RequestInit) => Promise<Response>

export const countFunctionInvocation = (cloudwatchClient: CloudWatchClient) =>
    (metric: MetricDefinition) =>
        <Input, Output>(fn: (...args: Array<Input>) => Output) =>
            (functionName: string) =>
                (...args: Array<Input>): Output => {
                    const result = fn(...args)
                    cloudwatchClient.send(new PutMetricDataCommand({
                        Namespace: metric.namespace,
                        MetricData: [
                            {
                                MetricName: metric.name,
                                Unit: 'Count',
                                Value: 1,
                                Dimensions: [{Name: 'Function', Value: functionName}],
                            },
                        ],
                    }))
                    return result
                }

export const incrementFunctionInvocationCount = (cloudwatchClient: CloudWatchClient) =>
    (metric: MetricDefinition) =>
        (functionName: string) =>
            async (count: number = 1): Promise<Optional<boolean>> => {
                return TryAsync(async () => {
                    return cloudwatchClient.send(new PutMetricDataCommand({
                        Namespace: metric.namespace,
                        MetricData: [
                            {
                                MetricName: metric.name,
                                Unit: 'Count',
                                Value: count,
                                Dimensions: [{Name: 'Function', Value: functionName}],
                            },
                        ],
                    })).then(_ => true)
                })

            }

export const countFetchStatus = (cloudwatchClient: CloudWatchClient) =>
    (fetch: Fetch) =>
        (metricConditions: Array<MetricCondition>) =>
            (functionName: string) =>
                async (input: RequestInfo, init?: RequestInit): Promise<Response> => {
                    return fetch(input, init).then(response => {

                        const metricPromises = metricConditions.reduce((metrics, metric) => {
                            if (metric.metricApplies(response)) {

                                return [
                                    ...metrics,
                                    cloudwatchClient.send(new PutMetricDataCommand({
                                        Namespace: metric.namespace,
                                        MetricData: [
                                            {
                                                MetricName: metric.name,
                                                Unit: 'Count',
                                                Value: 1,
                                                Dimensions: [{Name: 'Function', Value: functionName}],
                                            },
                                        ],
                                    })),
                                ]

                            }

                            return metrics

                        }, [])

                        return Promise.all(metricPromises).then(_ => response)

                    })
                }
