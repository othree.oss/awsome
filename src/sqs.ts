/** @module sqs */
import {
    SQSClient,
    SendMessageCommand,
    MessageAttributeValue,
    SendMessageBatchCommand,
    CreateQueueCommand,
    GetQueueAttributesCommand,
    DeleteQueueCommand,
    ReceiveMessageCommand,
    Message,
    DeleteMessageBatchCommand, ChangeMessageVisibilityCommand,
} from '@aws-sdk/client-sqs'
import {TryAsync, Optional} from '@othree.io/optional'

/**
 * Configuration options for sending messages to an SQS queue.
 * @interface
 * @property {string} QueueUrl - The URL of the queue to send messages to.
 */
export interface SQSConfiguration {
    readonly QueueUrl: string
}

/**
 * Creates a function that sends a message to an SQS queue.
 * @template T
 * @param {SQSClient} client - The SQS client used to send the message.
 * @param {SQSConfiguration} configuration - The configuration for sending messages.
 * @returns {Function} A series of functions that configure and send the message.
 * @async
 * @example
 * const sqsClient = new SQSClient({ region: 'us-east-1' })
 * const sendConfig = {
 *     QueueUrl: 'https://sqs.example.com/queue'
 * }
 * const sendMessageFunction = send(sqsClient, sendConfig)
 * const withMessageGroupId = (message) => 'group-id'
 * const withDeduplicationId = (message) => 'dedup-id'
 * const withAttributes = (message) => ({ AttributeName: 'AttributeValue' })
 * const sendPayload = { key: 'value' }
 * const result = await sendMessageFunction(withMessageGroupId)(withDeduplicationId)(withAttributes)(5)(sendPayload)
 * console.log('Message sent result:', result.get())
 */
export const send = <T>(client: SQSClient, configuration: SQSConfiguration) =>
    /**
     * Configures and sends a message to an SQS queue.
     * @param {function} withMessageGroupId - The function to extract the message group ID.
     * @returns {Function} A function that accepts optional configuration functions and delays to further configure the message.
     * @async
     */
    (withMessageGroupId?: (T) => string) =>
        /**
         * Configures the deduplication ID for the message.
         * @param {function} withDeduplicationId - The function to extract the deduplication ID.
         * @returns {Function} A function that accepts optional configuration functions and delays to further configure the message.
         */
        (withDeduplicationId?: (T) => string) =>
            /**
             * Configures custom attributes for the message.
             * @param {function} withAttributes - The function to extract message attributes.
             * @returns {Function} A function that accepts optional delay and sends the prepared payload.
             */
            (withAttributes?: (T) => { [key: string]: MessageAttributeValue }) =>
                /**
                 * Sets a delay for sending the message.
                 * @param {number} delay - The delay in seconds before sending the message.
                 * @returns {Function} A function that accepts the payload and sends the message.
                 * @async
                 */
                (delay?: number) =>
                    /**
                     * Sends a message to the configured SQS queue.
                     * @param {T} payload - The payload of the message to be sent.
                     * @returns {Promise<Optional<boolean>>} A promise that resolves to an optional boolean indicating the success of the send operation.
                     * @async
                     */
                    (payload: T): Promise<Optional<boolean>> => {
                        return TryAsync(async () => {
                            const sendMessageCommand = new SendMessageCommand({
                                QueueUrl: configuration.QueueUrl,
                                MessageBody: JSON.stringify(payload),
                                DelaySeconds: Optional(delay).orElse(undefined!),
                                MessageGroupId: Optional(withMessageGroupId).map(_ => _!(payload)).orElse(undefined!),
                                MessageDeduplicationId: Optional(withDeduplicationId).map(_ => _!(payload)).orElse(undefined!),
                                MessageAttributes: Optional(withAttributes).map(_ => _!(payload)).orElse(undefined!),
                            })

                            const sentMessageResponse = await client.send(sendMessageCommand)
                            console.debug('Sent message. Id: ', sentMessageResponse.MessageId, 'Sequence number: ', sentMessageResponse.SequenceNumber)
                            return true
                        })
                    }

/**
 * Represents the result of sending a batch of messages to an SQS queue.
 * @interface
 * @property {Array<string>} successful - An array of message IDs for successfully sent messages.
 * @property {Array<string>} failures - An array of message IDs for messages that failed to send.
 */
export interface SendBatchResult {
    readonly successful: Array<string>
    readonly failures: Array<string>
}

/**
 * Creates a function that sends a batch of messages to an SQS queue.
 * @template T
 * @param {SQSClient} client - The SQS client used to send the batch of messages.
 * @param {SQSConfiguration} configuration - The configuration for sending messages.
 * @returns {Function} A series of functions that configure and send the batch of messages.
 * @async
 * @example
 * const sqsClient = new SQSClient({ region: 'us-east-1' })
 * const sendConfig = {
 *     QueueUrl: 'https://sqs.example.com/queue'
 * }
 * const sendBatchFunction = sendBatch(sqsClient, sendConfig)
 * const withMessageId = (message) => message.id
 * const sendMessages = sendBatchFunction(withMessageId)
 * const payload = [{ id: '1', body: 'Message 1' }, { id: '2', body: 'Message 2' }]
 * const result = await sendMessages(payload)
 * console.log('Successful messages:', result.get().successful)
 * console.log('Failed messages:', result.get().failures)
 */
export const sendBatch = <T>(client: SQSClient, configuration: SQSConfiguration) =>
    /**
     * Configures and sends a batch of messages to an SQS queue.
     * @param {function} withMessageId - The function to extract the message ID from each message.
     * @returns {Function} A function that accepts optional configuration functions and delays to further configure the messages.
     * @async
     */
    (withMessageId: (T) => string) =>
        /**
         * Configures the message group ID for each message in the batch.
         * @param {function} withMessageGroupId - The function to extract the message group ID from each message.
         * @returns {Function} A function that accepts optional configuration functions and delays to further configure the messages.
         */
        (withMessageGroupId?: (T) => string) =>
            /**
             * Configures the deduplication ID for each message in the batch.
             * @param {function} withDeduplicationId - The function to extract the deduplication ID from each message.
             * @returns {Function} A function that accepts optional configuration functions and delays to further configure the messages.
             */
            (withDeduplicationId?: (T) => string) =>
                /**
                 * Configures custom attributes for each message in the batch.
                 * @param {function} withAttributes - The function to extract message attributes from each message.
                 * @returns {Function} A function that accepts optional delay and sends the prepared payload.
                 */
                (withAttributes?: (T) => { [key: string]: MessageAttributeValue }) =>
                    /**
                     * Sets a delay for sending the batch of messages.
                     * @param {number} delay - The delay in seconds before sending the batch.
                     * @returns {Function} A function that accepts the payload and sends the batch of messages.
                     * @async
                     */
                    (delay?: number) =>
                        /**
                         * Sends a batch of messages to the configured SQS queue.
                         * @param {Array<T>} payload - The array of messages to be sent in the batch.
                         * @returns {Promise<Optional<SendBatchResult>>} A promise that resolves to an optional SendBatchResult.
                         * @async
                         */
                        (payload: Array<T>): Promise<Optional<SendBatchResult>> => {
                            return TryAsync(async () => {
                                const sendMessageCommand = new SendMessageBatchCommand({
                                    QueueUrl: configuration.QueueUrl,
                                    Entries: payload.map(message => {
                                        return {
                                            Id: withMessageId(message),
                                            MessageBody: JSON.stringify(message),
                                            DelaySeconds: Optional(delay).orElse(undefined!),
                                            MessageGroupId: Optional(withMessageGroupId).map(_ => _!(message)).orElse(undefined!),
                                            MessageDeduplicationId: Optional(withDeduplicationId).map(_ => _!(message)).orElse(undefined!),
                                            MessageAttributes: Optional(withAttributes).map(_ => _!(message)).orElse(undefined!),
                                        }
                                    }),

                                })

                                const sentMessageResponse = await client.send(sendMessageCommand)

                                return {
                                    successful: Optional(sentMessageResponse.Successful!).map(_ => _!.map(successMessage => successMessage.Id!)).orElse([]),
                                    failures: Optional(sentMessageResponse.Failed!).map(_ => _!.map(successMessage => successMessage.Id!)).orElse([]),
                                }
                            })
                        }

/**
 * Request for creating a standard queue.
 * @interface
 * @property {string} queueName - The name of the queue to be created.
 */
export interface CreateStandardQueueRequest {
    readonly queueName: string
}

/**
 * Request for creating a FIFO queue.
 * @interface
 * @property {string} queueName - The name of the FIFO queue to be created.
 * @property {object} fifo - Configuration for the FIFO queue.
 * @property {boolean} fifo.contentBasedDeduplication - Whether content-based deduplication is enabled.
 * @property {'messageGroup' | 'queue'} fifo.deduplicationScope - The scope of deduplication.
 * @property {'perQueue' | 'perMessageGroupId'} fifo.throughputLimit - The throughput limit for the FIFO queue.
 */
export interface CreateFifoQueueRequest {
    readonly queueName: `${string}.fifo`
    readonly fifo: {
        contentBasedDeduplication: boolean
        deduplicationScope: 'messageGroup' | 'queue'
        throughputLimit: 'perQueue' | 'perMessageGroupId'
    }
}

/**
 * Union type representing the request to create a queue, which can be either standard or FIFO.
 */
export type CreateQueueRequest = CreateStandardQueueRequest | CreateFifoQueueRequest

/**
 * Represents an SQS queue.
 * @interface
 * @property {string} url - The URL of the SQS queue.
 * @property {string} arn - The ARN (Amazon Resource Name) of the SQS queue.
 */
export interface SqsQueue {
    readonly url: string
    readonly arn: string
}

/**
 * Checks whether a given request is for creating a FIFO queue.
 * @param {CreateQueueRequest} request - The request to be checked.
 * @returns {boolean} True if the request is for a FIFO queue, false otherwise.
 */
const isFifoRequest = (request: CreateQueueRequest): request is CreateFifoQueueRequest => {
    return 'fifo' in request
}

/**
 * Creates a function that creates an SQS queue based on the provided request.
 * @param {SQSClient} client - The SQS client used to create the queue.
 * @returns {Function} An asynchronous function that takes a CreateQueueRequest and returns a promise.
 * @async
 * @example
 * const sqsClient = new SQSClient({ region: 'us-east-1' });
 * const createQueueFunction = createQueue(sqsClient);
 * const standardQueueRequest = { queueName: 'my-standard-queue' };
 * const fifoQueueRequest = { queueName: 'my-fifo-queue.fifo', fifo: { contentBasedDeduplication: true, deduplicationScope: 'messageGroup', throughputLimit: 'perQueue' } };
 * const standardQueue = await createQueueFunction(standardQueueRequest);
 * console.log('Standard queue created:', standardQueue.get());
 * const fifoQueue = await createQueueFunction(fifoQueueRequest);
 * console.log('FIFO queue created:', fifoQueue.get());
 */
export const createQueue = (client: SQSClient) =>
    /**
     * Creates an SQS queue based on the provided request.
     * @param {CreateQueueRequest} request - The request to create an SQS queue.
     * @returns {Promise<Optional<SqsQueue>>} A promise that resolves to an optional SqsQueue object.
     * @async
     */
    async (request: CreateQueueRequest): Promise<Optional<SqsQueue>> => {
        return TryAsync(async () => {
            const response = await client.send(new CreateQueueCommand({
                QueueName: request.queueName,
                ...!isFifoRequest(request) ? {} : {
                    Attributes: {
                        FifoQueue: 'true',
                        ContentBasedDeduplication: String(request.fifo.contentBasedDeduplication),
                        DeduplicationScope: request.fifo.deduplicationScope,
                        FifoThroughputLimit: request.fifo.throughputLimit,
                    },
                },
            }))

            const attributesResponse = await client.send(new GetQueueAttributesCommand({
                QueueUrl: response.QueueUrl,
                AttributeNames: ['QueueArn'],
            }))

            return {
                url: response.QueueUrl!,
                arn: Optional(attributesResponse.Attributes).map(attributes => attributes.QueueArn).get(),
            }
        })
    }

/**
 * Represents a request to delete an SQS queue.
 * @interface
 * @property {string} queueUrl - The URL of the queue to be deleted.
 */
export interface DeleteQueueRequest {
    readonly queueUrl: string
}

/**
 * Creates a function that attempts to delete an SQS queue based on the provided request.
 * @param {SQSClient} client - The SQS client used to send the delete queue command.
 * @returns deleteQueue_1
 * @async
 * @example
 * const sqsClient = new SQSClient({ region: 'us-east-1' })
 * const deleteQueueFunction = deleteQueue(sqsClient)
 * const deleteRequest = { queueUrl: 'https://sqs.example.com/queue' }
 * const result = await deleteQueueFunction(deleteRequest)
 * if (result.isPresent) {
 *     console.log('Queue deleted successfully.')
 * } else {
 *     console.log('Queue deletion failed.')
 * }
 */
export const deleteQueue = (client: SQSClient) =>
    async (request: DeleteQueueRequest): Promise<Optional<boolean>> => {
        return TryAsync(async () => {
            await client.send(new DeleteQueueCommand({
                QueueUrl: request.queueUrl,
            }))

            return true
        })
    }

/**
 * Configuration options for receiving messages from an SQS queue.
 * Extends the base SQSConfiguration interface.
 * @interface
 * @extends SQSConfiguration
 * @property {number} maxNumberOfMessages - The maximum number of messages to receive in a single call.
 * @property {number} waitTimeSeconds - The time in seconds to wait for messages if the queue is empty.
 * @property {boolean} autoAcknowledgeMessages - If true, messages will be deleted from the queue
 */
export interface ReceiveMessagesConfiguration extends SQSConfiguration {
    readonly maxNumberOfMessages: number
    readonly waitTimeSeconds: number
    readonly autoAcknowledgeMessages: boolean
}

/**
 * Creates a function that receives messages from an SQS queue and transforms them.
 * @param {SQSClient} client - The SQS client used to receive messages.
 * @param {ReceiveMessagesConfiguration} configuration - The configuration for receiving messages.
 * @returns A higher-order function that takes a message transformation function and returns an asynchronous function.
 * @async
 * @example
 * const sqsClient = new SQSClient({ region: 'us-east-1' });
 * const receiveConfig = {
 *     QueueUrl: 'https://sqs.example.com/queue',
 *     maxNumberOfMessages: 10,
 *     waitTimeSeconds: 20
 * };
 * const receiveMessagesFunction = receiveMessages(sqsClient, receiveConfig);
 * const transformMessage = (message) => JSON.parse(message.Body);
 * const receivedTransformedMessages = await receiveMessagesFunction(transformMessage)();
 * console.log('Received and transformed messages:', receivedTransformedMessages.get());
 */
export const receiveMessages = (client: SQSClient, configuration: ReceiveMessagesConfiguration) =>
    <T>(transformMessage: (message: Message) => T) =>
        async (): Promise<Optional<Array<T>>> => {
            return TryAsync(async () => {
                const response = await client.send(new ReceiveMessageCommand({
                    QueueUrl: configuration.QueueUrl,
                    MaxNumberOfMessages: configuration.maxNumberOfMessages,
                    AttributeNames: ['All'],
                    WaitTimeSeconds: configuration.waitTimeSeconds,
                }))

                return Optional(response.Messages)
                    .mapAsync(async _ => {
                        if (configuration.autoAcknowledgeMessages) {
                            await client.send(new DeleteMessageBatchCommand({
                                QueueUrl: configuration.QueueUrl,
                                Entries: _.map(message => {
                                    return {
                                        Id: message.MessageId,
                                        ReceiptHandle: message.ReceiptHandle,
                                    }
                                }),
                            }))
                        }

                        return _.map(message => transformMessage(message))
                    })
                    .then(_ => _.orElse([]))
            })
        }


/**
 * Represents a request to get a message count of an SQS queue.
 * @interface
 * @property {string} queueUrl - The URL of the queue.
 */
export interface MessageCountRequest {
    readonly queueUrl: string
}

/**
 * Returns an asynchronous function that retrieves the approximate number of messages in an SQS queue.
 *
 * @param {SQSClient} client - The SQS client used to interact with Amazon SQS.
 * @returns {Function} An asynchronous function that accepts a `MessageCountRequest` in order to get the number of messages.
 */
export const getApproximateMessageCount = (client: SQSClient) =>
    /**
     * Retrieves the approximate number of messages in the specified SQS queue.
     *
     * @param {MessageCountRequest} messageCountRequest - The request to get the message count of a queue.
     * @returns {Promise<number>} A Promise resolving to the approximate number of messages
     */
    async (messageCountRequest: MessageCountRequest): Promise<Optional<number>> => {
        return TryAsync(async () => {
            const queueAttributes = await client.send(new GetQueueAttributesCommand({
                QueueUrl: messageCountRequest.queueUrl,
            }))

            if (queueAttributes.$metadata.httpStatusCode !== 200) {
                throw new Error('Cannot get queue attributes')
            }

            return Number(queueAttributes.Attributes?.['ApproximateNumberOfMessages'])
        })
    }

/**
 * Represents a request to change an SQS message visibility timeout.
 * @interface
 * @property {string} receiptHandle - The message receiptHandle.
 * @property {number} timeout - The new visibility timeout.
 */
export interface ChangeMessageVisibilityTimeoutRequest {
    readonly queue: string
    readonly receiptHandle: string
    readonly timeout: number
}

/**
 * Returns an asynchronous function that changes a message visibility timeout.
 *
 * @param {SQSClient} client - The SQS client used to interact with Amazon SQS.
 * @returns {Function} An asynchronous function that accepts a `ChangeMessageVisibilityTimeoutRequest` in order to change a message's visibility timeout.
 */
export const changeMessageVisibilityTimeout = (client: SQSClient) =>
    /**
     * Changes a message's visibility timeout
     *
     * @param {ChangeMessageVisibilityTimeoutRequest} request - The request to change the visibility timeout.
     * @returns {Promise<Optional<true>>} A Promise resolving to an Optional filled with true if successful
     */
    async (request: ChangeMessageVisibilityTimeoutRequest): Promise<Optional<true>> => {
        return TryAsync<true>(async () => {
            await client.send(new ChangeMessageVisibilityCommand({
                ReceiptHandle: request.receiptHandle,
                QueueUrl: request.queue,
                VisibilityTimeout: request.timeout,
            }))

            return true
        })
    }

/**
 * Represents a request to get a queue url.
 * @interface
 * @property {string} queueArn - Queue ARN.
 */
export interface GetQueueUrlInput {
    readonly queueArn: string
}

/**
 * Represents the response when getting a queue url.
 * @interface
 * @property {string} queueUrl - Queue url.
 */
export interface QueueUrlOutput {
    readonly queueUrl: string
}

/**
 * Returns a requested queue URL
 *
 * @param {GetQueueUrlInput} input - The queue arn.
 * @returns {QueueUrlOutput} The queue url
 */
export const getQueueUrl = (input: GetQueueUrlInput): QueueUrlOutput => {
    const [_, __, service, region, accountId, queueName] = input.queueArn.split(':')

    return {
        queueUrl: `https://${service}.${region}.amazonaws.com/${accountId}/${queueName}`,
    }
}
