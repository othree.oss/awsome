import {
    S3Client,
    GetObjectCommand,
    PutObjectCommand,
    ListObjectsCommand,
    DeleteObjectCommand,
    GetBucketLocationCommand
} from '@aws-sdk/client-s3'
import {TryAsync, Optional} from '@othree.io/optional'
import {BinaryObject} from './types'

export interface S3Configuration {
    readonly bucket: string
}

export interface S3Object {
    readonly key: string
    readonly size: number
    readonly lastModifiedDate: Date
}

export const get = (s3Client: S3Client, configuration: S3Configuration) => async (path: string): Promise<Optional<BinaryObject>> => {
    return TryAsync(async () => {
        const getCommand = new GetObjectCommand({
            Bucket: configuration.bucket,
            Key: path
        })

        const result = await s3Client.send(getCommand)

        return Object.freeze({
            body: result.Body!,
            contentType: result.ContentType,
            contentLength: result.ContentLength!
        })
    })
}

export const getPublicUrl = (s3Client: S3Client, configuration: S3Configuration) => async (key: string): Promise<Optional<string>> => {
    return TryAsync(async () => {
        const getCommand = new GetBucketLocationCommand({
            Bucket: configuration.bucket
        })

        const bucketLocationResult = await s3Client.send(getCommand)
        const bucketLocation = Optional(bucketLocationResult.LocationConstraint).map(_ => _!.toString())

        return `https://${configuration.bucket}.s3.${bucketLocation.orElse('us-east-1')}.amazonaws.com/${encodeURIComponent(key)}`
    })
}

export const list = (s3Client: S3Client, configuration: S3Configuration) => async (path: string): Promise<Optional<Array<S3Object>>> => {
    return TryAsync(async () => {
        const listObjectsCommand = new ListObjectsCommand({
            Bucket: configuration.bucket,
            Prefix: path
        })

        const objects = await s3Client.send(listObjectsCommand)

        return objects.Contents!.map(object => ({
            key: object.Key!,
            size: object.Size!,
            lastModifiedDate: object.LastModified!
        }))
    })
}

export const put = (s3Client: S3Client, configuration: S3Configuration) => async (path: string, data: BinaryObject): Promise<Optional<boolean>> => {
    return TryAsync(async () => {
        const putCommand = new PutObjectCommand({
            Body: data.body,
            Bucket: configuration.bucket,
            Key: path,
            ContentType: data.contentType,
            ContentLength: data.contentLength
        })

        await s3Client.send(putCommand)
        return true
    })
}

export const deleteObject = (s3Client: S3Client, configuration: S3Configuration) => async (path: string): Promise<Optional<boolean>> => {
    return TryAsync(async () => {
        const deleteCommand = new DeleteObjectCommand({
            Bucket: configuration.bucket,
            Key: path
        })

        await s3Client.send(deleteCommand)
        return true
    })
}
