import {SecretsManagerClient, GetSecretValueCommand, PutSecretValueCommand} from '@aws-sdk/client-secrets-manager'
import {TryAsync, Optional} from '@othree.io/optional'

const decoder = new TextDecoder()

export const get = (secretsManagerClient: SecretsManagerClient) => async (secretId: string): Promise<Optional<string>> => {
    return TryAsync(async () => {
        const getCommand = new GetSecretValueCommand({
            SecretId: secretId,
        })

        const secretResponse = await secretsManagerClient.send(getCommand)

        return secretResponse.SecretString ? secretResponse.SecretString : decoder.decode(secretResponse.SecretBinary)
    })
}

export const put = (secretsManagerClient: SecretsManagerClient) => (secretId: string) => async (secretValue: Record<string, string>): Promise<Optional<boolean>> => {
    return TryAsync(async () => {
        const putSecretCommand = new PutSecretValueCommand({
            SecretId: secretId,
            SecretString: JSON.stringify(secretValue),
        })
        await secretsManagerClient.send(putSecretCommand)
        return true
    })
}
