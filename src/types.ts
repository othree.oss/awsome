import {Readable} from 'stream'

export type BinaryObject = {
    readonly body: Readable | ReadableStream<any> | Blob | Uint8Array | Buffer
    readonly contentType?: string
    readonly contentLength: number
}